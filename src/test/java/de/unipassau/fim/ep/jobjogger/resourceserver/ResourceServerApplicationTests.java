package de.unipassau.fim.ep.jobjogger.resourceserver;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureTestDatabase
class ResourceServerApplicationTests {

	@Test
	void contextLoads() {
	}

}
