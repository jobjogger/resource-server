package de.unipassau.fim.ep.jobjogger.resourceserver.utilities;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.TextFileDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.utilities.TarGz;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.assertj.core.api.Assertions.assertThat;

public class TarGzTests {

    private final TextFileDto[] input = new TextFileDto[2];

    @Test
    public void compressDecompressTest() {
        input[0] = new TextFileDto("Dockerfile", "FROM python:3.7-alpine\\nWORKDIR /app\\nCOPY . .\\nCMD python code.py");
        input[1] = new TextFileDto("code.py", "print('Hello World')");
        ByteArrayInputStream compressed = new ByteArrayInputStream(TarGz.compress(input).toByteArray());
        TextFileDto[] output = TarGz.decompress(compressed);
        assertThat(output).isEqualTo(input);
    }


}
