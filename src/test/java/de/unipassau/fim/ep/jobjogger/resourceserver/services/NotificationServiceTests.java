package de.unipassau.fim.ep.jobjogger.resourceserver.services;

import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.ForbiddenOperationException;
import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.IllegalParameterException;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.Notification;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.NotificationType;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.repos.NotificationRepo;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.MailService;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.NotificationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTests {

    @InjectMocks
    private NotificationService notificationService;

    @Mock
    private NotificationRepo notificationRepo;

    @Mock
    private MailService mailService;

    private final NotificationType MOCK_NOTIFICATIONTYPE = NotificationType.JOB_SUCCEEDED;
    private final String MOCK_USER = "vali";
    private final String MOCK_JOB_NAME = "HELLO";
    private final String MOCK_IMAGE_NAME = "TESTIMAGE";
    private final String MOCK_FORBIDDEN_USER = "helmut";
    private final Long MOCK_JOB_ID = 12L;
    private final Image MOCK_IMAGE = new Image();
    private final Job MOCK_JOB = new Job(MOCK_JOB_NAME, MOCK_USER, MOCK_IMAGE);
    private final Notification MOCK_NOTIFICATION = new Notification(MOCK_NOTIFICATIONTYPE, MOCK_USER, "", MOCK_JOB_ID
            ,MOCK_JOB_NAME, MOCK_IMAGE_NAME);
    private final List<Long> MOCK_IDS = new LinkedList<>();


    @Before
    public void init() {
        MOCK_NOTIFICATION.setId(10L);
        MOCK_IDS.add(10L);
        MOCK_JOB.setId(MOCK_JOB_ID);
    }

    @Test
    public void createNotificationTest() {
        when(notificationRepo.save(any(Notification.class))).thenReturn(MOCK_NOTIFICATION);
        boolean thrown_should_not = false;
        try {
            notificationService.createNotification(MOCK_NOTIFICATIONTYPE, MOCK_USER, "", MOCK_JOB);
        } catch (IllegalParameterException exc) {
            thrown_should_not = true;
        }
        assertThat(thrown_should_not).isFalse();

        boolean thrown_should = false;
        try {
            notificationService.createNotification(MOCK_NOTIFICATIONTYPE, null, "", MOCK_JOB);
        } catch (IllegalParameterException exc) {
            thrown_should = true;
        }

        assertThat(thrown_should).isTrue();

    }

    @Test
    public void markAsSeenTest() {
        List<Notification> db_response = new LinkedList<>();
        db_response.add(MOCK_NOTIFICATION);
        when(notificationRepo.findAllById(MOCK_IDS)).thenReturn(db_response);
        boolean thrown_should_not = false;
        try {
            notificationService.markAsSeen(MOCK_IDS, MOCK_USER);
        } catch (ForbiddenOperationException exc) {
            thrown_should_not = true;
        }
        assertThat(thrown_should_not).isFalse();

        boolean thrown_should = false;
        try {
            notificationService.markAsSeen(MOCK_IDS, MOCK_FORBIDDEN_USER);
        } catch (ForbiddenOperationException exc) {
            thrown_should = true;
        }
        assertThat(thrown_should).isTrue();
    }
}
