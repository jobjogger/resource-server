package de.unipassau.fim.ep.jobjogger.resourceserver.services;

import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.EntityInUseException;
import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.EntityNotFoundException;
import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.ForbiddenOperationException;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.EnvVar;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.repos.ImageRepo;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.repos.JobRepo;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.JobService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Testing of dockerimages service. Methods that just pass to the repo are not being tested.
 */
@RunWith(MockitoJUnitRunner.class)
public class JobServiceTest {
    @Mock
    private ImageRepo imageRepo;

    @Mock
    private JobRepo jobRepo;

    @InjectMocks
    private JobService jobService;

    private Image image;
    private Job job;
    private static final String MOCK_IMAGE_ID = "vali101:mytest:1.0";
    private static final String MOCK_JOB_USER = "vali";
    private static final String MOCK_JOB_NAME = "dockerimage";
    private JobDto jobDto;

    @Before
    public void init() {
        job = new Job("job", MOCK_JOB_USER, this.image);
        job.setId(123L);
        when(jobRepo.findById(123L))
                .thenReturn(java.util.Optional.of(job));
        image = new Image(MOCK_JOB_USER, MOCK_IMAGE_ID);
        when(imageRepo.findById(MOCK_IMAGE_ID))
                .thenReturn(java.util.Optional.of(image));

        jobDto = new JobDto();
        EnvVar ev = new EnvVar();
        jobDto.setName("dto");
        List<EnvVar> envVars = new ArrayList<>();
        envVars.add(ev);
        jobDto.setEnvironmentVariables(envVars);
    }

    /**
     * Tests the creation of a new job.
     */
    @Test
    public void createJobTest() {
        when(jobRepo.save(any(Job.class))).thenReturn(new Job(MOCK_JOB_NAME, MOCK_JOB_USER, image));
        Job job = jobService.createJob(MOCK_JOB_NAME, MOCK_JOB_USER, image.getId(), null, null);
        assertThat(job.getName()).isEqualTo(MOCK_JOB_NAME);
    }

    /**
     * Tests if creating a new job is not possible, because the dockerimage does not exist.
     */
    @Test
    public void createJobTestNotFound() {
        boolean thrown = false;
        try {
            Job job = jobService.createJob("job", MOCK_JOB_USER, "wrong_id", null, null);
        } catch (EntityNotFoundException exc) {
            thrown = true;
        }
        assertThat(thrown).isTrue();
    }

    /**
     * Tests if creating a new job is illegal.
     */
    @Test
    public void createJobTestIllegal() {

        boolean thrown = false;
        try {
            jobService.createJob(MOCK_JOB_NAME, "peter", image.getId(), null, null);
        } catch (ForbiddenOperationException exc) {
            thrown = true;
        }
        assertThat(thrown).isTrue();
    }

    /**
     * Tests if a job will be deleted properly.
     */
    @Test
    public void testDeleteJob() {
        when(jobRepo.existsById(any(Long.class))).thenReturn(true);
        boolean thrown = false;
        try {
            jobService.deleteJob(job.getUserId(), job);
        } catch (EntityNotFoundException | EntityInUseException e) {
            thrown = true;
        }
        assertThat(thrown).isFalse();
        assertThat(job == null);
    }
    /**
     * Tests if a job, that will be deleted exists.
     */
    @Test
    public void deleteJobTestNotFound() {
        boolean thrown = false;
        Job unknownJob = new Job();
        unknownJob.setId(147L);
        when(jobRepo.existsById(any(Long.class))).thenReturn(false);
        try {
            jobService.deleteJob(job.getUserId(), unknownJob);
        } catch (EntityNotFoundException e) {
            thrown = true;
        }
        assertThat(thrown).isTrue();
        assertThat(job != null);
    }

    /**
     * Tests if it is possible to delete the job.
     */
    @Test
    public void deleteJobInUse() {
        when(jobRepo.existsById(any(Long.class))).thenReturn(true);
        boolean thrown = false;
        try {
            job.setJobStatus(JobStatus.RUNNING);
            jobService.deleteJob(job.getUserId(), job);
        } catch (EntityInUseException e) {
            thrown = true;
        }
        assertThat(thrown).isTrue();
        assertThat(job != null);
    }

    /**
     * Service test for posting the logs of a job.
     */
    @Test
    public void testPostJobLogs() {
        boolean thrown = false;
        try {
            jobService.postJobLogs(job.getId(),  MOCK_JOB_USER, "test");
        } catch (EntityInUseException | EntityNotFoundException e) {
            thrown = true;
        }
        assertThat(thrown).isFalse();
    }

    /**
     * Tests if the job whose logs are posted, does exist.
     */
    @Test
    public void testPostJobLogsNotFound() {
        boolean thrown = false;
        try {
            jobService.postJobLogs(1L,  MOCK_JOB_USER, "test");
        } catch (EntityNotFoundException e) {
            thrown = true;
        }
        assertThat(thrown).isTrue();
    }

    /**
     * Tests if the user owns the job whose logs are posted.
     */
    @Test
    public void testPostJobLogsIllegal() {
        boolean thrown = false;
        try {
            jobService.postJobLogs(job.getId(), "eva", "test");
        } catch (ForbiddenOperationException e) {
            thrown = true;
        }
        assertThat(thrown).isTrue();
    }

    /**
     * Service test for updating a job if it does not exist.
     */
    @Test
    public void testUpdateJobNotFound() {
        boolean thrown = false;
        try {
            jobService.updateJob(1L, jobDto);
        } catch (EntityNotFoundException e) {
            thrown = true;
        }
        assertThat(thrown).isTrue();
    }

    /**
     * Service test for updating a job if it is currently in use.
     * That means the job is either running or scheduled.
     */
    @Test
    public void testUpdateJobInUse() {
        job.setJobStatus(JobStatus.PENDING);
        boolean thrown = false;
        try {
            jobService.updateJob(job.getId(), jobDto);
        } catch (EntityInUseException e) {
            thrown = true;
        }
        assertThat(thrown).isTrue();
    }

    /**
     * Service test for updating the job's status.
     * The job should have a new/updated status.
     */
    @Test
    public void testUpdateJobStatus() {
        job.setJobStatus(JobStatus.PENDING);
        boolean thrown = false;
        try {
            jobService.updateJobStatus(job.getId(), MOCK_JOB_USER, JobStatus.RUNNING);
        } catch (EntityNotFoundException | ForbiddenOperationException e) {
            thrown = true;
        }
        assertThat(thrown).isFalse();
        assertThat(job.getJobStatus() == JobStatus.RUNNING);

    }

    /**
     * Service test for updating a job's status if the user does not own this job.
     */
    @Test
    public void testUpdateJobStatusForbidden() {
        job.setJobStatus(JobStatus.RUNNING);
        boolean thrown = false;
        try {
            jobService.updateJobStatus(job.getId(), "eva", JobStatus.FAILED);
        } catch (ForbiddenOperationException e) {
            thrown = true;
        }
        assertThat(thrown).isTrue();
        assertThat(job.getJobStatus() == JobStatus.RUNNING);
    }

    /**
     * Service Test for updating the job's status if this job does not exist.
     */
    @Test
    public void testUpdateJobStatusNotFound() {
        job.setJobStatus(JobStatus.RUNNING);
        boolean thrown = false;
        try {
            jobService.updateJobStatus(1L, MOCK_JOB_USER, JobStatus.FAILED);
        } catch (EntityNotFoundException e) {
            thrown = true;
        }
        assertThat(thrown).isTrue();
        assertThat(job.getJobStatus() == JobStatus.RUNNING);
    }

    /**
     * Service test for getting the logs of a job if the job does not exist.
     */
    @Test
    public void testGetLogsOfJobNotFound() {
        boolean thrown = false;
        try {
            jobService.getLogsOfJob(1L, 0);
        } catch (EntityNotFoundException e) {
            thrown = true;
        }
        assertThat(thrown).isTrue();
    }
}