package de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDtoLogs;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.JobService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@WithMockUser(username = "vali")
@AutoConfigureMockMvc
public class ClusterControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    private final Image image = new Image();
    private final Job job = new Job();
    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private JobService jobService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        image.setId("vali101:mytest:1.0");
        image.setName("dockerimage");
        job.setName("Jobby");
        job.setId(12345L);
        job.setUserId("vali");
        job.setImage(image);
        job.setLogs("test");
    }

    @Test
    public void testPostJobLogs() throws Exception {
        Job returnedJob = new Job("job", "vali", image);
        returnedJob.setId(job.getId());
        JobDtoLogs requestJob = new JobDtoLogs();
        requestJob.setName("job");
        requestJob.setId(returnedJob.getId());
        requestJob.setUserId(returnedJob.getUserId());
        requestJob.setLogs("test");
        System.out.println(objectMapper.writeValueAsString(requestJob));
        mockMvc.perform(post("/cluster/users/" + returnedJob.getUserId() + "/jobs/" + returnedJob.getId() + "/logs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestJob))
                .characterEncoding("utf-8"))
                .andDo((print()))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateJobStatus() throws Exception {
        job.setJobStatus(JobStatus.CANCELED);
        Job returnedJob = this.job;
        JobDto requestJob = new JobDto();
        requestJob.setName("job");
        requestJob.setId(returnedJob.getId());
        requestJob.setUserId(returnedJob.getUserId());
        requestJob.setStatus(returnedJob.getJobStatus());
        System.out.println(objectMapper.writeValueAsString(requestJob));
        mockMvc.perform(post("/cluster/users/" + returnedJob.getUserId() + "/jobs/" + returnedJob.getId() + "/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestJob))
                .characterEncoding("utf-8"))
                .andDo((print()))
                .andExpect(status().isOk());
    }
}
