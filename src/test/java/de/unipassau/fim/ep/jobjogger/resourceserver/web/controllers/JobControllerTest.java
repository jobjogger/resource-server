package de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.JobService;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@WithMockUser(username = "vali")
@AutoConfigureMockMvc
public class JobControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    private final Image image = new Image();
    private final Job job = new Job();
    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private JobService jobService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        image.setId("vali101:mytest:1.0");
        image.setName("dockerimage");
        job.setName("Jobby");
        job.setId(12345L);
        job.setUserId("vali");
        job.setImage(image);
        job.setLogs("test");
    }

    /**
     * Tests the Controller for getting all jobs by user.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testJobGet() throws Exception {
        List<Job> jobList = new LinkedList<>();
        jobList.add(job);

        when(jobService.getAllJobsByUser(any(String.class), any(Pageable.class)))
                .thenReturn(jobList);
        this.mockMvc.perform(get("/users/" + job.getUserId() + "/jobs"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(job.getName())))
                .andExpect(jsonPath("$[0].id", is(job.getId().intValue())))
                .andExpect(jsonPath("$[0].userId", is(job.getUserId())));
    }

    /**
     * Tests the controller of starting a job.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testStartJob() throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        Job returnedJob = this.job;
        ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
        when(jobService.getJobById(returnedJob.getId()))
                .thenReturn(job);
        JobDto jobDto = modelMapper.map(job, JobDto.class);
        when(jobService.clusterStartRequest("vali", jobDto))
        .thenReturn(response);
        mockMvc.perform(patch("/users/" + job.getUserId() + "/jobs/" + job.getId() + "/start"))
                .andDo(print())
                .andExpect(status().isOk());
    }


    /**
     * Tests the controller for getting all Job statuses by Ids.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testJobStatusesGet() throws Exception {
        job.setJobStatus(JobStatus.RUNNING);
        List<Job> jobList = new LinkedList<>();
        jobList.add(job);

        String userId = job.getUserId();
        List<Long> jobIds = Arrays.stream(new Long[]{job.getId()}).collect(Collectors.toList());

        when(jobService.getJobStatusesByIds(userId, jobIds))
                .thenReturn(jobList);

        System.out.println(jobService.getJobStatusesByIds(userId, jobIds));

        this.mockMvc.perform(get("/users/" + job.getUserId() + "/jobs/statuses")
                            .param("ids", StringUtils.join(jobIds, ',')))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(job.getId().intValue())))
                .andExpect(jsonPath("$[0].status", is("RUNNING")));
    }


    /**
     * Tests the controller for getting all running jobs by user.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testJobGetRunning() throws Exception {
        job.setJobStatus(JobStatus.RUNNING);
        List<Job> jobList = new LinkedList<>();
        jobList.add(job);

        when(jobService.getUsersJobsPerStatus(any(JobStatus.class), any(String.class), any(Pageable.class)))
                .thenReturn(jobList);

        this.mockMvc.perform(get("/users/" + job.getUserId() + "/jobs/status/running"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(job.getName())))
                .andExpect(jsonPath("$[0].status", is("RUNNING")))
                .andExpect(jsonPath("$[0].userId", is(job.getUserId())));

    }

    /**
     * Tests the controller for getting all pending jobs by user.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testJobGetPending() throws Exception {
        job.setJobStatus(JobStatus.PENDING);
        List<Job> jobList = new LinkedList<>();
        jobList.add(job);

        when(jobService.getUsersJobsPerStatus(any(JobStatus.class), any(String.class), any(Pageable.class)))
                .thenReturn(jobList);

        this.mockMvc.perform(get("/users/" + job.getUserId() + "/jobs/status/pending"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(job.getName())))
                .andExpect(jsonPath("$[0].status", is("PENDING")))
                .andExpect(jsonPath("$[0].userId", is(job.getUserId())));
    }

    /**
     * Tests the controller for getting all succeeded jobs by user.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testJobGetSucceeded() throws Exception {
        job.setJobStatus(JobStatus.SUCCEEDED);

        List<Job> jobs = new LinkedList<>();
        jobs.add(job);
        when(jobService.getUsersJobsPerStatus(any(JobStatus.class), any(String.class), any(Pageable.class)))
                .thenReturn(jobs);

        this.mockMvc.perform(get("/users/" + job.getUserId() + "/jobs/status/succeeded"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(job.getName())))
                .andExpect(jsonPath("$[0].id", is(job.getId().intValue())))
                .andExpect(jsonPath("$[0].status", is(JobStatus.SUCCEEDED.toString())));
    }

    /**
     * Tests the controller for getting all failed jobs by user.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testJobGetFailed() throws Exception {
        job.setJobStatus(JobStatus.FAILED);
        List<Job> jobs = new LinkedList<>();
        jobs.add(job);
        when(jobService.getUsersJobsPerStatus(any(JobStatus.class), any(String.class), any(Pageable.class)))
                .thenReturn(jobs);
        this.mockMvc.perform(get("/users/" + job.getUserId() + "/jobs/status/failed"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(job.getName())))
                .andExpect(jsonPath("$[0].id", is(job.getId().intValue())))
                .andExpect(jsonPath("$[0].status", is(JobStatus.FAILED.toString())));
    }

    /**
     * Tests the controller for getting all jobs by user, if the user does not own the jobs.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testJobGetProhibited() throws Exception {
        this.mockMvc.perform(get("/users/peter/jobs"))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    /**
     * Tests the controller for creating a job.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testPostJob() throws Exception {
        String prio = "default";
        Job returnedJob = new Job("job", "vali", image);
        returnedJob.setPriority(prio);
        returnedJob.setJobStatus(JobStatus.NONE);
        returnedJob.setId(1L);
        Timestamp timestamp = Timestamp.from(Instant.MIN);
        returnedJob.setCreateTime(timestamp);
        when(jobService.createJob("job", "vali", image.getId(),null, prio))
                .thenReturn(returnedJob);

        JobDto requestJob = new JobDto();
        requestJob.setImageId(image.getId());
        requestJob.setPriority(prio);
        requestJob.setName("job");
        System.out.println(objectMapper.writeValueAsString(requestJob));
        mockMvc.perform(post("/users/vali/jobs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestJob))
                .characterEncoding("utf-8"))
                .andDo((print()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("job"))
                .andExpect(jsonPath("$.imageId").value(image.getId()))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.status").value(JobStatus.NONE.toString()))
                .andExpect(jsonPath("$.meta.createTime").isString())
                .andExpect(jsonPath("$.priority").value(prio));
    }

    /**
     * Tests the controller for getting the logs of a job.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testGetLogs() throws Exception {
        job.setLogs("test");
        when(jobService.getLogsOfJob(job.getId(), 0))
        .thenReturn(job.getLogs());
        mockMvc.perform(get("/users/" + job.getUserId() + "/jobs/" + job.getId() + "/logs"))
        .andDo(print())
        .andExpect(status().isOk());
    }

    /**
     * Tests the controller for getting the logs of a job, if the user does not own a job.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testGetLogsProhibited() throws Exception {
        when(jobService.getLogsOfJob(job.getId(), 0))
                .thenReturn(job.getLogs());
        mockMvc.perform(get("/users/eva/jobs/" + job.getId() + "/logs"))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }



    /**
     * Tests the controller for creating a new job but the creation is prohibited
     * because the user does not own the job.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testPostJobProhibited() throws Exception {
        JobDto requestJob = new JobDto();
        requestJob.setImageId(image.getId());
        requestJob.setName("job");
        System.out.println(objectMapper.writeValueAsString(requestJob));
        mockMvc.perform(post("/users/peter/jobs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestJob))
                .characterEncoding("utf-8"))
                .andDo((print()))
                .andExpect(status().isUnauthorized());
    }

    /**
     * Tests the controller for updating a job.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testUpdateJob() throws Exception {
        ArrayList<String> ev = new ArrayList<>();
        ev.add("test");
        when(jobService.createJob("job1", "vali", image.getId(),null, null))
                .thenReturn(new Job("job1", "vali", image));
        MockHttpServletRequestBuilder request = generateUpdateRequest("/users/vali/jobs/12345L", "job2", ev);
        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest());
    }



    /**
     * Tests the controller for canceling a job.
     * @throws Exception If test is broken.
     */
    @Test
    public void testCancelJob() throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        Job returnedJob = this.job;
        ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
        when(jobService.getJobById(returnedJob.getId()))
                .thenReturn(job);
        JobDto jobDto = modelMapper.map(job, JobDto.class);
        when(jobService.clusterDeleteRequest("vali", jobDto))
                .thenReturn(response);
        mockMvc.perform(patch("/users/" + job.getUserId() + "/jobs/" + job.getId() + "/cancel"))
                .andDo((print()))
                .andExpect(status().isOk());
    }

    @Test
    public void testCancelJobUnauthorized() throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        Job returnedJob = this.job;
        ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
        when(jobService.getJobById(returnedJob.getId()))
                .thenReturn(job);
        JobDto jobDto = modelMapper.map(job, JobDto.class);
        when(jobService.clusterDeleteRequest("eva", jobDto))
                .thenReturn(response);
        mockMvc.perform(patch("/users/eva/jobs/" + job.getId() + "/cancel"))
                .andDo((print()))
                .andExpect(status().isUnauthorized());
    }


    /**
     * Tests the controller for updating a job, but the request has wrong inputs.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testUpdateJobWrongInput() throws Exception {
        ArrayList<String> ev = new ArrayList<>();
        ev.add("test");
        when(jobService.createJob("job1", "vali", image.getId(),null, null))
                .thenReturn(new Job("job1", "vali", image));
        MockHttpServletRequestBuilder request = generateUpdateRequest("/users/vali/jobs/12345L", "", ev);
        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    /**
     * Tests the controller for updating a job, but the user does not own the job.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testUpdateJobWhileProhibited() throws Exception {
        ArrayList<String> ev = new ArrayList<>();
        ev.add("test");
        when(jobService.createJob("job1", "vali", image.getId(),null, null))
                .thenReturn(new Job("job1", "vali", image));
        MockHttpServletRequestBuilder request = generateUpdateRequest("/users/eva/jobs/12345L", "job2", ev);
        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    /**
     * Tests the controller for deleting a job.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testDeleteJob() throws Exception {
        Job jobToDelete = this.job;
        when(jobService.getJobById(jobToDelete.getId())).thenReturn(jobToDelete);
        this.mockMvc.perform(delete("/users/" + jobToDelete.getUserId() + "/jobs/" + jobToDelete.getId()))
        .andDo(print())
        .andExpect(status().isOk());
    }

    /**
     * Tests the controller for deleting a job when the user wants
     * to delete a job he does not own.
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testDeleteJobProhibited() throws Exception {
        Job jobToDelete = this.job;
        this.mockMvc.perform(delete("/users/eva/jobs/" + jobToDelete.getId()))
        .andDo(print())
        .andExpect(status().isUnauthorized());
    }

    private static MockHttpServletRequestBuilder generateUpdateRequest(String url, String name, ArrayList<String> environmentVariable) throws Exception {
        String jsonString = (new ObjectMapper()).writeValueAsString(new HashMap<String, String>() {
            {
                put("name", name);
            }

            {
                put("environmentVariable", String.valueOf(environmentVariable));
            }
        });
        return put(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(jsonString);
    }
    }



