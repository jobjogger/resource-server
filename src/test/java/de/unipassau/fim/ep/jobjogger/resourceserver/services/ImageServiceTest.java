package de.unipassau.fim.ep.jobjogger.resourceserver.services;

import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.EntityInUseException;
import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.EntityNotFoundException;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.FullImageMeta;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.PushFailDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.PushReturnEntity;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.PushSuccessDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.repos.ImageRepo;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.DockerimageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Testing of dockerimages service. Methods that just pass to the repo are not being tested.
 */
@RunWith(MockitoJUnitRunner.class)
public class ImageServiceTest {
    @Mock
    private ImageRepo imageRepo;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private DockerimageService dockerimageService;

    @Value("${host.registration-server}")
    private String registrationServerHost;

    private static final String MOCK_IMAGE_ID = "vali01:mytest:1.0";
    private static final String MOCK_IMAGE_USER = "vali";
    private static final String MOCK_IMAGE_NAME = "fibonacci";
    private static final MockMultipartFile MOCK_MULTIPART_FILE = new MockMultipartFile("file", "d".getBytes());
    private static final FullImageMeta MOCK_IMAGE_META = new FullImageMeta();
    private static final PushSuccessDto[] MOCK_PUSH_SUCCESS = new PushSuccessDto[]{(new PushSuccessDto(MOCK_IMAGE_NAME,MOCK_IMAGE_USER, "http://asdf.de", MOCK_IMAGE_META))};
    private static final PushFailDto[] MOCK_PUSH_FAIL = new PushFailDto[]{};
    private static final PushReturnEntity MOCK_PUSH_RETURN_ENTITY = new PushReturnEntity(MOCK_PUSH_SUCCESS, MOCK_PUSH_FAIL);



    @Test
    public void createDockerimageTest(){
        when(imageRepo.save(any(Image.class))).thenReturn(new Image(MOCK_IMAGE_USER, MOCK_IMAGE_NAME));
        ResponseEntity<PushReturnEntity> responseEntity = new ResponseEntity<>(MOCK_PUSH_RETURN_ENTITY, HttpStatus.OK);
        when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                any(HttpMethod.class),
                any(),
                ArgumentMatchers.<Class<PushReturnEntity>> any())).thenReturn(responseEntity);
        System.out.println("Test"+ responseEntity.toString());
        PushReturnEntity responseFromService = dockerimageService.createDockerimage(MOCK_MULTIPART_FILE, MOCK_IMAGE_USER);
        assertThat(responseEntity.getStatusCode().is2xxSuccessful());
        assertThat(responseEntity.getBody()).isEqualTo(responseFromService);
    }
    @Test
    public void readOneDockerimageByIdTest(){
        when(imageRepo.findById(MOCK_IMAGE_ID))
                .thenReturn(java.util.Optional.of(new Image(MOCK_IMAGE_NAME, MOCK_IMAGE_ID)));
        Image found = dockerimageService.readOneDockerimageById(MOCK_IMAGE_ID);

        assertThat(found.getId()).isEqualTo(MOCK_IMAGE_ID);

    }
    @Test
    public void readOneDockerimageByIdTestAndFail(){
        when(imageRepo.findById(any(String.class)))
                .thenReturn(Optional.empty());
        boolean thrown = false;
        try {
            Image found = dockerimageService.readOneDockerimageById("wrong_id");
        }catch (EntityNotFoundException exc){
            thrown =true;
        }
       assertThat(thrown).isTrue();

    }

    @Test
    public void deleteDockerImageWithId(){
        Image image = new Image(MOCK_IMAGE_NAME, MOCK_IMAGE_ID);

        when(imageRepo.findById(MOCK_IMAGE_ID))
                .thenReturn(java.util.Optional.of(image));
        when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                any(HttpMethod.class),
                any(),
                ArgumentMatchers.<Class<String>> any()))
                .thenReturn(new ResponseEntity<>(HttpStatus.OK));

        boolean thrown = false;

        try {
            dockerimageService.deleteDockerimage(MOCK_IMAGE_ID);
        }catch (RuntimeException exc){
            exc.printStackTrace();
            thrown =true;
        }
        assertThat(thrown).isFalse();
    }

    @Test
    public void deleteDockerImageWithIdNotFound(){
        boolean thrown = false;

        try {
            dockerimageService.deleteDockerimage("wrong_id");
        }catch (EntityNotFoundException exc){
            thrown =true;
        }
        assertThat(thrown).isTrue();
    }

    @Test
    public void deleteDockerImageWithIdRunningJobs(){
        Image image = new Image(MOCK_IMAGE_NAME, MOCK_IMAGE_ID);
        Job job = new Job();
        job.setImage(image);
        job.setJobStatus(JobStatus.RUNNING);
        List<Job> jobs = new LinkedList<>();
        jobs.add(job);
        image.setJobs(jobs);
        when(imageRepo.findById(MOCK_IMAGE_ID))
                .thenReturn(java.util.Optional.of(image));
        boolean thrown = false;

        try {
            dockerimageService.deleteDockerimage(MOCK_IMAGE_ID);
        }catch (EntityInUseException exc){
            thrown =true;
        }
        assertThat(thrown).isTrue();
    }

}
