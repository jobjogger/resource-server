package de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.Notification;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.NotificationType;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.NotificationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@WithMockUser(username = "vali")
@AutoConfigureMockMvc
public class NotificationControllerTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private NotificationService notificationService;

    private final String MOCK_USER = "vali";
    private final String MOCK_JOB_NAME = "HELLO";
    private final String MOCK_IMAGE_NAME = "TESTIMAGE";
    private final String INBOX_USER_ENDPOINT = "/inbox/" + MOCK_USER;
    private final NotificationType MOCK_NOTIFICATIONTYPE = NotificationType.JOB_SUCCEEDED;
    private final Long MOCK_JOB_ID = 12L;
    private final Notification MOCK_NOTIFICATION = new Notification(MOCK_NOTIFICATIONTYPE, MOCK_USER,"", MOCK_JOB_ID,
            MOCK_JOB_NAME, MOCK_IMAGE_NAME);
    private final List<Notification> MOCK_NOTIFICATIONS_LIST = new LinkedList<>();

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        MOCK_NOTIFICATIONS_LIST.add(MOCK_NOTIFICATION);
    }
    @Test
    public void getNotificationsTest() throws Exception{
        when(notificationService.getAllNotifications(any(String.class), any(Pageable.class))).thenReturn(MOCK_NOTIFICATIONS_LIST);

        mockMvc.perform(get(INBOX_USER_ENDPOINT))
                .andDo((print()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].notificationType").value(MOCK_NOTIFICATIONTYPE.toString()))
                .andExpect(jsonPath("$[0].userId").value(MOCK_USER));
    }

    /*
    Test omitted: MarkAsSeen just delegates to service layer, where a test exists.
     */
}
