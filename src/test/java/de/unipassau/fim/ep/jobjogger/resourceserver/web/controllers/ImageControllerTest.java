package de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.FullImageMeta;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.PushFailDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.PushReturnEntity;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.PushSuccessDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.DockerimageService;
import org.codehaus.jackson.map.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureTestDatabase
@WithMockUser(username = "vali")
public class ImageControllerTest {
    private MockMvc mockMvc;

    private static final String MOCK_DOCKERIMAGE_OWNER = "vali";
    private static final String MOCK_DOCKERIMAGE_NOT_OWNER = "peter";

    private static final String MOCK_DOCKERIMAGE_NAME = "hello-world:latest";
    private static final byte[] MOCK_DOCKERIMAGE_DATA = "data".getBytes();
    private static final MockMultipartFile MOCK_MULTIPART_FILE = new MockMultipartFile("file",MOCK_DOCKERIMAGE_NAME,"application/x-tar",MOCK_DOCKERIMAGE_DATA);
    private static final FullImageMeta MOCK_IMAGE_META = new FullImageMeta();
    private static final PushSuccessDto[] MOCK_PUSH_SUCCESS = new PushSuccessDto[]{(new PushSuccessDto(MOCK_DOCKERIMAGE_NAME,MOCK_DOCKERIMAGE_OWNER, "http://asdf.de", MOCK_IMAGE_META))};
    private static final PushFailDto[] MOCK_PUSH_FAIL = new PushFailDto[]{};
    private static final PushReturnEntity MOCK_PUSH_RETURN_ENTITY = new PushReturnEntity(MOCK_PUSH_SUCCESS, MOCK_PUSH_FAIL);


    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private DockerimageService dockerimageService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testDockerimageGet() throws Exception {
        List<Image> list = new LinkedList<>();
        Image testImage = new Image(MOCK_DOCKERIMAGE_NAME, MOCK_DOCKERIMAGE_OWNER);
        Timestamp timestamp = Timestamp.from(Instant.MIN);
        testImage.setUploadTime(timestamp);
        list.add(testImage);
        when(dockerimageService.readAllDockerimagesByUser(any(String.class), any(Pageable.class)))
                .thenReturn(list);
        this.mockMvc.perform(get("/users/vali/dockerimages"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(testImage.getName())))
                .andExpect(jsonPath("$[0].userId", is(testImage.getUserId())))
                .andExpect(jsonPath("$[0].meta.uploadTime", Matchers.containsString(timestamp.toString().substring(1,10))));
    }

    @Test
    public void testDockerimageGetProhibited() throws Exception {
        this.mockMvc.perform(get("/users/peter/dockerimages"))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testDockerimagePost() throws Exception {
        when(dockerimageService.createDockerimage(MOCK_MULTIPART_FILE, MOCK_DOCKERIMAGE_OWNER))
                .thenReturn(MOCK_PUSH_RETURN_ENTITY);
        mockMvc.perform(multipart("/users/"+ MOCK_DOCKERIMAGE_OWNER + "/dockerimages")
                .file(MOCK_MULTIPART_FILE))
                .andDo((print()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success[0].image", is(MOCK_DOCKERIMAGE_NAME)))
                .andExpect(jsonPath("$.success[0].user", is(MOCK_DOCKERIMAGE_OWNER)));

    }

    @Test
    public void testDockerimagePostInvalidFile() throws Exception {
        when(dockerimageService.createDockerimage(MOCK_MULTIPART_FILE, MOCK_DOCKERIMAGE_OWNER))
                .thenReturn(MOCK_PUSH_RETURN_ENTITY);
        mockMvc.perform(multipart("/users/"+ MOCK_DOCKERIMAGE_OWNER + "/dockerimages")
                .file(new MockMultipartFile("file", "hallo.jpg", "imgae/jpg", MOCK_DOCKERIMAGE_DATA))
                .param("prefix", MOCK_DOCKERIMAGE_NAME))
                .andDo((print()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDockerimagePostProhibited() throws Exception {
        when(dockerimageService.createDockerimage(MOCK_MULTIPART_FILE, MOCK_DOCKERIMAGE_OWNER))
                .thenReturn(MOCK_PUSH_RETURN_ENTITY);
        mockMvc.perform(multipart("/users/"+ MOCK_DOCKERIMAGE_NOT_OWNER+ "/dockerimages")
                .file(new MockMultipartFile("file", "hallo.txt", "text/plain", MOCK_DOCKERIMAGE_DATA))
                .param("prefix", MOCK_DOCKERIMAGE_NAME))
                .andDo((print())).andExpect(status().isUnauthorized());
    }

    @Test
    public void testUpdateDockerimage() throws Exception {
        when(dockerimageService.createDockerimage(MOCK_MULTIPART_FILE, MOCK_DOCKERIMAGE_OWNER))
                .thenReturn(MOCK_PUSH_RETURN_ENTITY);

        MockHttpServletRequestBuilder request =
                generateUpdateRequest("/users/" + MOCK_DOCKERIMAGE_OWNER + "/dockerimages/1", "Dockerimage 2");

        this.mockMvc.perform(request).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testUpdateDockerimageWhenProhibited() throws Exception {
        when(dockerimageService.createDockerimage(MOCK_MULTIPART_FILE, MOCK_DOCKERIMAGE_OWNER))
                .thenReturn(MOCK_PUSH_RETURN_ENTITY);

        MockHttpServletRequestBuilder request =
                generateUpdateRequest("/users/" + MOCK_DOCKERIMAGE_NOT_OWNER + "/dockerimages/1", "Dockerimage 2");

        this.mockMvc.perform(request).andDo(print()).andExpect(status().isUnauthorized());
    }

    @Test
    public void testUpdateDockerimageWithIllegalParameters() throws Exception {
        when(dockerimageService.createDockerimage(MOCK_MULTIPART_FILE, MOCK_DOCKERIMAGE_OWNER))
                .thenReturn(MOCK_PUSH_RETURN_ENTITY);

        // TODO: Passes also with different id
        MockHttpServletRequestBuilder request =
                generateUpdateRequest("/users/" + MOCK_DOCKERIMAGE_OWNER + "/dockerimages/1", "");

        this.mockMvc.perform(request)
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    private static MockHttpServletRequestBuilder generateUpdateRequest(String url, String name) throws Exception {
        String jsonString = (new ObjectMapper()).writeValueAsString(new HashMap<String, String>() {{
            put("name", name);
        }});

        return put(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(jsonString);
    }

    @Test
    public void testDeleteDockerimage() throws Exception{
        mockMvc.perform(delete("/users/" + MOCK_DOCKERIMAGE_OWNER + "/dockerimages/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteDockerimageProhibited() throws Exception{
        mockMvc.perform(delete("/users/" + MOCK_DOCKERIMAGE_NOT_OWNER + "/dockerimages/1"))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
}
