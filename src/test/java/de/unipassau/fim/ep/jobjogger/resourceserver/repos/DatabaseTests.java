package de.unipassau.fim.ep.jobjogger.resourceserver.repos;


import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.repos.ImageRepo;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.repos.JobRepo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase
public class DatabaseTests {

    @Autowired
    private JobRepo jobRepo;
    @Autowired
    private ImageRepo imageRepo;
    private Image imageInDB;

    @Autowired
    private TestEntityManager entityManager;

    @Before
    public void setUp() {
        Image image = new Image();
        image.setName("Pi-Calc");
        image.setId("Pi-Calc");
        image.setUserId("PeterProfi");
        byte[] randomBytes = new byte[1024];
        new Random().nextBytes(randomBytes);
        imageInDB = entityManager.persist(image);

        entityManager.flush();

        Job job = new Job("Pi-Test-RunNr1", "PeterProfi", imageInDB);
        job.setJobStatus(JobStatus.RUNNING);
        job.setPriority("default");
        entityManager.persist(job);
        entityManager.flush();
    }

    @Test
    public void findingDockerimagesByUser() {
        PageRequest pageReq = PageRequest.of(0, 10, Sort.unsorted());
        Image savedImage = imageRepo.findAllByUserIdOrderByUploadTimeDesc("PeterProfi", pageReq).iterator().next();
        assertThat(savedImage.getName()).isEqualTo("Pi-Calc");
    }

    @Test
    public void findingJobsByUser() {
        PageRequest pageReq = PageRequest.of(0, 10, Sort.unsorted());
        Job savedJob = jobRepo.findAllByUserId("PeterProfi", pageReq).iterator().next();
        assertThat(savedJob.getName()).isEqualTo("Pi-Test-RunNr1");
    }

    @Test
    public void findingJobByStatusAndUser() {
        PageRequest pageReq = PageRequest.of(0, 10, Sort.unsorted());
        Job savedJob = jobRepo.findAllByUserIdAndJobStatusEquals("PeterProfi", JobStatus.RUNNING, pageReq).iterator().next();
        Boolean isEmpty = jobRepo.findAllByUserIdAndJobStatusEquals("PeterProfi", JobStatus.SUCCEEDED, pageReq).iterator().hasNext();

        assertThat(savedJob.getName()).isEqualTo("Pi-Test-RunNr1");
        assertThat(isEmpty);
    }


    @Test
    public void checkDeletion() {
        imageRepo.deleteById(imageInDB.getId());
        Image found = imageRepo.findById(imageInDB.getId()).orElse(null);
        assertThat(found).isNull();
    }

    @Test
    public void saveJob() {
        Image image = new Image("vali", "val01/test");
        Job job = new Job("jobby", "vali", image);
        job.setLogs("Hello World!");
        Job persisted = jobRepo.save(job);

        Job found = jobRepo.findById(persisted.getId()).orElse(null);
        assertThat(found.getLogs().equals(persisted.getLogs()));
        assertThat(job.getLogs().equals(found.getLogs()));
    }
}
