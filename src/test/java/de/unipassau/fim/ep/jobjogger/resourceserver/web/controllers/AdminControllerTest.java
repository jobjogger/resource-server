package de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.CancelJobDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.JobService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Timestamp;
import java.time.Instant;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@WithMockUser(username = "vali", roles = "ADMIN")
@AutoConfigureMockMvc
public class AdminControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    private final Image image = new Image();
    private final Job job = new Job();

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private JobService jobService;

    private final String POST_ADMIN_JOB_ENDPOINT = "/users/vali/jobs";
    private final String MOCK_REASON = "took too long";
    private final String MOCK_DEFAULT_PRIORITY = "default";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        image.setId("vali101:mytest:1.0");
        image.setName("dockerimage");
        job.setName("Jobby");
        job.setId(12345L);
        job.setUserId("vali");
        job.setImage(image);
        job.setLogs("test");
    }

    /**
     * Tests the Job creation with priority and admin role
     *
     * @throws Exception If test is broken.
     */
    @Test
    public void testpostNewPriorityJob() throws Exception {
        String prio = "system";
        Job returnedJob = new Job("job", "vali", image);
        returnedJob.setPriority(prio);
        returnedJob.setJobStatus(JobStatus.NONE);
        returnedJob.setId(1L);
        Timestamp timestamp = Timestamp.from(Instant.MIN);
        returnedJob.setCreateTime(timestamp);
        when(jobService.createJob("job", "vali", image.getId(), null, prio))
                .thenReturn(returnedJob);

        JobDto requestJob = new JobDto();
        requestJob.setImageId(image.getId());
        requestJob.setPriority(prio);
        requestJob.setName("job");
        System.out.println(objectMapper.writeValueAsString(requestJob));
        mockMvc.perform(post(POST_ADMIN_JOB_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestJob))
                .characterEncoding("utf-8"))
                .andDo((print()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("job"))
                .andExpect(jsonPath("$.imageId").value(image.getId()))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.status").value(JobStatus.NONE.toString()))
                .andExpect(jsonPath("$.meta.createTime").isString())
                .andExpect(jsonPath("$.priority").value(prio));
    }

    /**
     * Test the Job creation with the wrong role
     *
     * @throws Exception If test is broken.
     */
    @Test
    @WithMockUser(username = "vali", roles = "USER")
    public void testPostNewPriorityPodUnauthorized() throws Exception {
        String prio = "system";
        JobDto requestJob = new JobDto();
        requestJob.setImageId(image.getId());
        requestJob.setPriority(prio);
        requestJob.setName("job");

        Job returnedJob = new Job("job", "vali", image);
        returnedJob.setPriority(MOCK_DEFAULT_PRIORITY);
        returnedJob.setJobStatus(JobStatus.NONE);
        returnedJob.setId(1L);
        when(jobService.createJob(any(String.class), any(String.class), any(String.class), any(), any()))
                .thenReturn(returnedJob);

        System.out.println(objectMapper.writeValueAsString(requestJob));
        mockMvc.perform(post(POST_ADMIN_JOB_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestJob))
                .characterEncoding("utf-8"))
                .andDo((print()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.priority").value(MOCK_DEFAULT_PRIORITY));
    }

 /*
    The reason for block comment is the following:
    In our admin controller the username is injected through the token by using the @AuthenticationPrinciple annotation.
    For some reason I (Peter) did not manage to get the tests working for several hours, while the injection works in
    real life. After investing 3 hours I do not think it is important enough due to the fact that the logic of the
    controller is very basic. In addition, the authentication is mocked and would only be tested by integration tests.


    @Test
    @WithMockUser(username = "vali", roles = "ADMIN")
    public void testCancelOwningJobWithAdminRole() throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        Job returnedJob = this.job;
        ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
        when(jobService.getJobById(returnedJob.getId()))
                .thenReturn(job);
        JobDto jobDto = modelMapper.map(job, JobDto.class);
        when(jobService.clusterDeleteRequest("eva", jobDto))
                .thenReturn(response);
        mockMvc.perform(patch("/admins/jobs/" + job.getId())
                .content(objectMapper.writeValueAsString(MOCK_CANCEL_DTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo((print()))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "eva", roles = "ADMIN")
    public void testCancelNotOwningJobWithAdminRole() throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        Job returnedJob = this.job;
        ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
        when(jobService.getJobById(returnedJob.getId()))
                .thenReturn(job);
        JobDto jobDto = modelMapper.map(job, JobDto.class);
        when(jobService.clusterDeleteRequest("eva", jobDto))
                .thenReturn(response);
        mockMvc.perform(patch("/admins/jobs/" + job.getId())
                .content(objectMapper.writeValueAsString(MOCK_CANCEL_DTO))
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(Encoder.UTF_8))
                .andDo(print())
                .andExpect(status().isOk());
    }*/

    @Test
    @WithMockUser(username = "eva", roles = "ROLE")
    public void testCancelNotOwningJobWithUserRole() throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        Job returnedJob = this.job;
        ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
        when(jobService.getJobById(returnedJob.getId()))
                .thenReturn(job);
        JobDto jobDto = modelMapper.map(job, JobDto.class);
        when(jobService.clusterDeleteRequest("eva", jobDto))
                .thenReturn(response);
        mockMvc.perform(patch("/admins/jobs/" + job.getId()))
                .andDo((print()))
                .andExpect(status().is4xxClientError());
    }

}
