package de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception thrown when deletion of job is not possible due to an illegal state.
 */
@ResponseStatus(HttpStatus.CONFLICT)
@Slf4j
public class JobAlreadyRunningException extends RuntimeException{
    public JobAlreadyRunningException(String msg) {
        super(msg);
        log.error("JobAlreadyRunningException: " + this.getMessage() );
    }
}
