package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Dto that contains notification ids that should be marked as seen.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarkNotificationDto {
    List<Long> ids;
}
