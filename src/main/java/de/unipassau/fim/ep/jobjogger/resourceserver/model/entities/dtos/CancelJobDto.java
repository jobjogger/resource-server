package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CancelJobDto {
    String reason;
}
