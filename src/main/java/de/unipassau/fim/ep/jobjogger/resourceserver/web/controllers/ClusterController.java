package de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDtoLogs;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.JobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

/**
 * These are the Controllers, which are used to communicate with the cluster.
 */
@RequestMapping("/cluster")
@RestController
@Slf4j
public class ClusterController {

    private final JobService jobService;

    public ClusterController(JobService jobService) {
        this.jobService = jobService;
    }


    /**
     * Controller for updating the job's status. Also if the updated status is either
     * CANCELED, FAILED or SUCCEEDED.
     *
     * @param username The user who owns the job.
     * @param id       The Job-ID.
     * @param job      The updated status of the job.
     * @return 200 OK HTTP Status-Code.
     */
    @PostMapping("/users/{username}/jobs/{id}/status")
    public ResponseEntity<String> updateJobStatus(@PathVariable @AuthenticationPrincipal String username,
                                                  @PathVariable Long id, @RequestBody JobDto job) {
        log.info("Update status request received for job with id " + id + ". Status:" + job.getStatus());
        jobService.updateJobStatus(id, username, job.getStatus());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Controller for posting the job's logs.
     *
     * @param username The user who owns the job.
     * @param id       The Job-ID.
     * @param job      The logs of this job.
     * @return 200 OK HTTP Status-Code.
     */
    @PostMapping("/users/{username}/jobs/{id}/logs")
    public ResponseEntity<String> postJobLogs(@PathVariable @AuthenticationPrincipal String username,
                                              @PathVariable Long id,
                                              @RequestBody JobDtoLogs job) {
        jobService.postJobLogs(id, username, job.getLogs());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
