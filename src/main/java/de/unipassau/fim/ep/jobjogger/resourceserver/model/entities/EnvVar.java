package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@AllArgsConstructor
@Data
@NoArgsConstructor
public class EnvVar {
    String name;
    String value;
}
