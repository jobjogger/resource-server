package de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception thrown when forwarding an image failed.
 */
@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
@Slf4j
public class ForwardFileException extends RuntimeException{
    public ForwardFileException(String msg){
        super(msg);
        log.error("ForwardFileException: " + msg);
    }
}
