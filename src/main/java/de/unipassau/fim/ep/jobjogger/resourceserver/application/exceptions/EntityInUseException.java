package de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception thrown when deletion of a image and its jobs is not possible due to a running job.
 * The exception will also be thrown when a running job wants to be deleted.
 */
@ResponseStatus(HttpStatus.CONFLICT)
@Slf4j
public class EntityInUseException extends RuntimeException {
    public EntityInUseException(String msg) {
        super(msg);
        log.error("EntityInUseException: " + this.getMessage() );
    }
}
