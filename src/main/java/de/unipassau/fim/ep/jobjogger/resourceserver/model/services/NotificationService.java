package de.unipassau.fim.ep.jobjogger.resourceserver.model.services;

import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.ForbiddenOperationException;
import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.IllegalParameterException;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.Notification;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.NotificationType;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.repos.NotificationRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * This class organises READ, UPDATE and CREATE operations for notifications.
 */
@Service
@Slf4j
public class NotificationService {
    private final NotificationRepo notificationRepo;
    private final MailService mailService;

    public NotificationService(NotificationRepo notificationRepo, MailService mailService) {
        this.notificationRepo = notificationRepo;
        this.mailService = mailService;
    }

    /**
     * Persist notification in the database.
     *
     * @param notificationType the type of the notification describing the reason to notify the user,
     * @param userId           username.
     * @param reason           the reason given by the admin, only needed when the admin cancels the job.
     * @param job              the job the notification is associated with.
     */
    public void createNotification(NotificationType notificationType, String userId, String reason, Job job) {
        Long jobId = job.getId();
        if (notificationType == null || userId == null || jobId == null){
            throw new IllegalParameterException("NOTIFICATION CREATION ERROR: " +
                    "Notification type, userId or jobID are null.");
        }
        Notification notification = new Notification(notificationType, userId, reason, job.getId(), job.getName(),
                job.getImage().getName());
        log.info("Notification of type " + notificationType.toString() + " saved.");
        notificationRepo.save(notification);
        mailService.sendMail(notificationType, userId, reason, job);
    }

    /**
     * Returns all notification by user from the database.
     *
     * @param userID   user identifier.
     * @param pageable allowing pagination.
     * @return all notifications.
     */
    public List<Notification> getAllNotifications(String userID, Pageable pageable) {
        return iterableToList(notificationRepo.findAllByUserIdOrderByTimestampDesc(userID, pageable));
    }

    /**
     * Marking ta list of notification as seen.
     *
     * @param ids    the ids of the notifications to be marked.
     * @param userId user identifier.
     */
    public void markAsSeen(List<Long> ids, String userId) {
        Iterable<Notification> notifications = notificationRepo.findAllById(ids);
        for (Notification notification : notifications) {
            // Only notifications belonging to the user can be marked as seen.
            if (userId.equals(notification.getUserId())) {
                notification.setSeen(true);
            } else {
                throw new ForbiddenOperationException("Notifications not belonging to the user may not be marked as seen.");
            }
        }
        notificationRepo.saveAll(notifications);
    }

    private static List<Notification> iterableToList(Iterable<Notification> it) {
        List<Notification> list = new LinkedList<>();
        for (Notification notification : it) {
            list.add(notification);
        }
        return list;
    }
}
