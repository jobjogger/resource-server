package de.unipassau.fim.ep.jobjogger.resourceserver.model.services;

import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.AuthRequestFailedException;
import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.EntityNotFoundException;
import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.ForbiddenOperationException;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.NotificationType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

/**
 * Service that sends an email to the user's email when a job is terminated.
 */
@Service
@Slf4j
public class MailService {


    @Value("${host.auth-server}")
    private String authServerHost;

    @Value("${microservice-secret.secret-token}")
    private String principalRequestValue;

    private final JavaMailSender javaMailSender;
    private final RestTemplate restTemplate;
    private final String FROM_EMAIL = "noreply@jobjogger";

    public MailService(JavaMailSender javaMailSender, RestTemplate restTemplate) {
        this.javaMailSender = javaMailSender;
        this.restTemplate = restTemplate;
    }


    /**
     * Sends an email notification to the user when his job is terminated.
     *
     * @param job The jobDto of the job that has been terminated.
     */
    public void sendMail(NotificationType notificationType, String userId, String reason, Job job) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(FROM_EMAIL);
        simpleMailMessage.setTo(authGetRequest(userId));
        simpleMailMessage.setSubject("INFO: The status of Job with ID: " + job.getId() + " has changed.");
        simpleMailMessage.setText(setMessageBody(notificationType,userId, reason, job));
        javaMailSender.send(simpleMailMessage);
        log.info("Mail has been send successfully.");
    }

    /**
     * Sets a specific email message with further infos about the job
     * for the user who will be notified about a terminated job.
     *
     * @param job The job that is terminated.
     * @return The message body.
     */
    private String setMessageBody(NotificationType notificationType, String userId, String reason, Job job) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hello ").append(userId).append("!\n");
        stringBuilder.append("Your job called ").append(job.getName()).append(" created at ").append(job.getCreateTime()).append(" ");
        switch(notificationType) {
            case JOB_SUCCEEDED:
            case JOB_FAILED:
                stringBuilder.append(job.getJobStatus());
                break;
            case JOB_CANCELED:
                stringBuilder.append(job.getJobStatus()).append(". Reason: ").append(reason).append(".");
                break;
        }
        stringBuilder.append("\n Have a nice day, \n Your JobJogger Team");
        return stringBuilder.toString();
    }

    /**
     * Creates a GET request from this server to the authentication-server to get the mail address of
     * the user who will be notified.
     *
     * @return The mail address as String.
     */
    private String authGetRequest(String userId) {
        String authServerUrl = authServerHost + "/auth/mail/" + userId;
        log.info("Executing GET request to auth-Server." +authServerUrl+ " ");
        HttpEntity<String> request = new HttpEntity<>(getHeaders());
        ResponseEntity<String> response;
        try {
            response = restTemplate.exchange(authServerUrl, HttpMethod.GET, request, String.class);
        } catch (HttpStatusCodeException exc) {
            if (exc.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new EntityNotFoundException("E-Mail adress of user " + userId + " not found.");
            }
            if (exc.getStatusCode() == HttpStatus.FORBIDDEN) {
                throw new ForbiddenOperationException("The request to the auth-server is forbidden.");
            }
            else {
                throw new AuthRequestFailedException("E-Mail request returned " + exc.getStatusCode().toString());
            }
        }
        log.info("E-Mail received from auth-server: " + response.getBody());
        return response.getBody();
    }

    /**
     * Creates a request header, containing the micro-service secret.
     *
     * @return The created header.
     */
    private HttpHeaders getHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer " + principalRequestValue);
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }
}
