package de.unipassau.fim.ep.jobjogger.resourceserver.application.config;

import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.JobAlreadyRunningException;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.PushReturnEntity;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.DockerimageService;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.JobService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Initialises the database or performs a stress test, if the necessary environment-variables are set.
 */
@Component
@Slf4j
public class DbInitializer implements CommandLineRunner {

    @Value("${spring.application.init-db}")
    private boolean initDb;
    @Value("${spring.application.stress-test}")
    private boolean stressTest;

    private final List<Job> allJobs = new LinkedList<>();
    private final DockerimageService dockerimageService;
    private final JobService jobService;
    private final ModelMapper modelMapper;

    public DbInitializer(DockerimageService dockerimageService, JobService jobService, ModelMapper modelMapper) {
        this.dockerimageService = dockerimageService;
        this.jobService = jobService;
        this.modelMapper = modelMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run(String... args) {
        try {
            sleep(3);
            initialiseDatabase("vali", false);

            if (initDb || stressTest) {
                String user = "peter";
                initialiseDatabase(user, true);
            }
        } catch (Exception exc1) {
            log.error("Exception during db initialisation.");
            exc1.printStackTrace();
        }
    }

    private void initialiseDatabase(String user, boolean createJobs) throws IOException {
        InputStream file = new ClassPathResource("images/hello-world.tar").getInputStream();
        PushReturnEntity pushReturnEntity = dockerimageService.createDockerimage(file, user);
        int max_jobs = 40;
        if (stressTest) {
            max_jobs = 60;
        }
        if (pushReturnEntity.getSuccess().length > 0 && createJobs) {
            String image_id = pushReturnEntity.getSuccess()[0].getImage();
            for (int i = 0; i < max_jobs; i++) {
                createJob("Job_" + i, user, image_id);
            }
        }
        if (initDb && !stressTest) {
            setJobStatuses();
        }
        if (stressTest) {
            startJobs();
        }
    }

    private void createJob(String name,
                           String userId,
                           String dockerimageId) {
        Job job = jobService.createJob(name, userId, dockerimageId, new LinkedList<>(), "default");
        allJobs.add(job);
    }

    private void setJobStatuses() {
        for (Job job : allJobs) {
            jobService.updateJobStatus(job.getId(), job.getUserId(), getRandomStatus());
        }
    }

    private JobStatus getRandomStatus() {
        JobStatus[] jobStatuses = JobStatus.values();
        Random random = new Random();
        return jobStatuses[random.nextInt(jobStatuses.length)];


    }

    private void startJobs() {
        for (Job job : allJobs) {
            if (job.getJobStatus().equals(JobStatus.NONE)) {
                try {
                    jobService.clusterStartRequest(job.getUserId(), modelMapper.map(job, JobDto.class));
                } catch (HttpStatusCodeException exc) {
                    System.out.println("Exception with status code '" + exc.getStatusCode() + "' occurred during DB-init\n" +
                            "Msg: " + exc.getMessage());
                } catch (JobAlreadyRunningException exc) {
                    System.out.println("Job already running");
                }
                sleep(1);
            }
        }
    }

    private void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            log.info("got interrupted");
        }
    }
}
