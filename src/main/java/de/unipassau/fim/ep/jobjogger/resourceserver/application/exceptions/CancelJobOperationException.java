package de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception if canceling a job has not been successful.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
@Slf4j
public class CancelJobOperationException extends RuntimeException {
    public CancelJobOperationException(String msg) {
        super(msg);
        log.error("CancelJobOperationException: " + this.getMessage() );
    }
}
