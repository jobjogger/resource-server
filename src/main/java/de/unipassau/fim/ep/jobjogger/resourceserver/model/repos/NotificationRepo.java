package de.unipassau.fim.ep.jobjogger.resourceserver.model.repos;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for database-independent access to notifications.
 */
public interface NotificationRepo extends JpaRepository<Notification, Long> {
    Page<Notification> findAllByUserIdOrderByTimestampDesc(String user, Pageable pageable);
}