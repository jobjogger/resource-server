package de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers;

import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.IllegalParameterException;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.ImageDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.TextFileDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.PushReturnEntity;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.DockerimageService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.resource.ResourceUrlProvider;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers.ControllerUtilityClass.convertImageList;
import static de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers.ControllerUtilityClass.imagePageableSort;

/**
 * This controller is responsible for handling dockerimages as well as storing and finding them in the database.
 */
@RestController
public class DockerimageController {
    private final List<String> supportedFiles = new LinkedList<>(Arrays.asList("application/x-tar", "application/x-gzip"));

    private final DockerimageService dockerimageService;

    private final ModelMapper modelMapper;

    public DockerimageController(DockerimageService dockerimageService, ModelMapper modelMapper) {
        this.dockerimageService = dockerimageService;
        this.modelMapper = modelMapper;
    }

    /**
     * Creates a new Dockerimage with the file in byte format.
     *
     * @param username The User who owns the image.
     * @param file     Binary file which is the Dockerimage.
     * @return The Dockerimage that was created.
     */
    @PreAuthorize("#username == authentication.name")
    @PostMapping(value = "/users/{username}/dockerimages")
    public PushReturnEntity createDockerimage(@PathVariable @AuthenticationPrincipal String username,
                                              @NotNull @RequestParam MultipartFile file) {
        if (!isValidFile(file)) {
            throw new IllegalParameterException("File-type not supported. Please upload a .tar or a tar.gz file.");
        }
        return dockerimageService.createDockerimage(file, username);
    }

    /**
     * Controller that receives files where a docker-image should be built from.
     *
     * @param username of the user uploading the files.
     * @param files    the text-files.
     * @return response from registration service.
     */
    @PreAuthorize("#username == authentication.name")
    @PostMapping(value = "/users/{username}/dockerimages/build/**")
    public PushReturnEntity buildDockerimage(@PathVariable @AuthenticationPrincipal String username,
                                             @NotNull @RequestBody TextFileDto[] files,
                                             HttpServletRequest request) {
        String imageId = getRequestTrail(request);
        return dockerimageService.buildDockerImage(files, username, imageId);

    }

    /**
     * Get the ziped text-files from the db, uncompress it and send them back to the user.
     *
     * @param username of the user who requests the files.
     * @param request  the request object.
     * @return text-files containing code.
     */
    @GetMapping(value = "/users/{username}/dockerimages/build/**")
    @PreAuthorize("#username == authentication.name or hasRole('ADMIN')")
    public TextFileDto[] getSourceFiles(@PathVariable @AuthenticationPrincipal String username,
                                        HttpServletRequest request) {
        String imageId = getRequestTrail(request);
        return dockerimageService.getSourceArchive(imageId);
    }

    private boolean isValidFile(MultipartFile file) {
        if (file.isEmpty()) {
            throw new IllegalParameterException("The file must not be empty.");
        }

        if (file.getContentType() != null) {
            for (String type : supportedFiles) {
                if (file.getContentType().equals(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Reads a Dockerimage by its Id.
     *
     * @param username The User who owns the image.
     * @param request  The request object.
     * @return A HTTP 200 Success response if the Dockerimage was updated successfully.
     */
    @PreAuthorize("#username == authentication.name or hasRole('ADMIN')")
    @GetMapping("/users/{username}/dockerimages/**")
    public ImageDto readOneDockerimageById(@PathVariable @AuthenticationPrincipal String username,
                                           HttpServletRequest request) {
        String imageId = getRequestTrail(request);
        return modelMapper.map(dockerimageService.readOneDockerimageById(imageId), ImageDto.class);
    }

    /**
     * Reads all Dockerimages owned by a user.
     *
     * @param username The User who owns the image.
     * @param page     Number of pages indexed by 1.
     * @param size     Number of entries per page.
     * @param sort     Sorting of entries.
     * @return All Dockerimages owned by a user.
     */
    @PreAuthorize("#username == authentication.name or hasRole('ADMIN')")
    @GetMapping("/users/{username}/dockerimages")
    public List<ImageDto> readAllDockerimagesByUser(@PathVariable @AuthenticationPrincipal String username,
                                                    @RequestParam(required = false, defaultValue = "1") Integer page,
                                                    @RequestParam(required = false, defaultValue = "10") Integer size,
                                                    @RequestParam(required = false, defaultValue = "upload") String sort) {
        Sort pageableSort = imagePageableSort(sort);
        Pageable pageable = PageRequest.of(page - 1, size, pageableSort);

        return convertImageList(modelMapper, dockerimageService.readAllDockerimagesByUser(username, pageable));
    }

    /**
     * Updates a Dockerimage by its Id.
     *
     * @param username The User who owns the image.
     * @param request  The request object.
     * @param data     The new data of the Dockerimage.
     * @return A HTTP 200 Success response if the Dockerimage was updated successfully.
     */
    @PreAuthorize("#username == authentication.name")
    @PutMapping("/users/{username}/dockerimages/**")
    public ResponseEntity<Long> updateDockerimageById(@PathVariable @AuthenticationPrincipal String username,
                                                      @NotNull @RequestBody ImageDto data,
                                                      HttpServletRequest request) {
        String imageId = getRequestTrail(request);
        if (data.getName() == null || data.getName().isEmpty()) {
            throw new IllegalParameterException("The name must not be empty.");
        }

        dockerimageService.updateDockerimage(imageId, data);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Deletes a Dockerimage by its Id.
     *
     * @param username The User who owns the image.
     * @return A HTTP 200 Success response if the Dockerimage was deleted successfully.
     */
    @PreAuthorize("#username == authentication.name or hasRole('ADMIN')")
    @DeleteMapping("/users/{username}/dockerimages/**")
    public ResponseEntity<Long> deleteDockerimageById(@PathVariable @AuthenticationPrincipal String username,
                                                      HttpServletRequest request) {
        String imageId = getRequestTrail(request);
        dockerimageService.deleteDockerimage(imageId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private String getRequestTrail(HttpServletRequest request) {
        ResourceUrlProvider urlProvider = (ResourceUrlProvider) request
                .getAttribute(ResourceUrlProvider.class.getCanonicalName());
        return urlProvider.getPathMatcher().extractPathWithinPattern(
                String.valueOf(request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE)),
                String.valueOf(request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE)));
    }
}


