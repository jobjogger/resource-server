package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.EnvVar;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This includes the metadata sent  by the registration service. Not all infos here should be given to the user.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FullImageMeta {
    private Timestamp created;
    private String comment;
    private List<EnvVar> env;
    private List<String> cmd;
    private String workdir;
    private List<String> entrypoint;
    private Map<String, String> labels;
    private String dockerversion;
    private String author;
    private String architecture;
    private Integer size;
    private Integer virtualsize;

    public FullImageMeta(Timestamp created, String comment, String[] env, List<String> cmd, String workdir, List<String> entrypoint, Map<String, String> labels, String dockerversion, String author, String architecture, Integer size, Integer virtualsize) {
        this.created = created;
        this.comment = comment;
        setEnv(env);
        this.cmd = cmd;
        this.workdir = workdir;
        this.entrypoint = entrypoint;
        this.labels = labels;
        this.dockerversion = dockerversion;
        this.author = author;
        this.architecture = architecture;
        this.size = size;
        this.virtualsize = virtualsize;
    }

    public void setEnv(String[] envs) {
        this.env = toKeyValue(envs);
    }

    private List<EnvVar> toKeyValue(String[] envs){
        List<EnvVar> envVars = new ArrayList<>(envs.length);
        for (String strEnv : envs) {
            String[] tupel = strEnv.split("=");
            EnvVar envVar;
            if (tupel.length == 2) {
                envVar = new EnvVar(tupel[0], tupel[1]);
            } else if (tupel.length == 1) {
                envVar = new EnvVar();
                envVar.setName(tupel[0]);
            } else {
                envVar = new EnvVar();
                envVar.setName(strEnv);
            }
            envVars.add(envVar);
        }
        return envVars;
    }

}
