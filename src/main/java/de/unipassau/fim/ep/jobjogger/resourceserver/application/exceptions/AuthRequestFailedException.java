package de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
@Slf4j
public class AuthRequestFailedException extends RuntimeException {
    public AuthRequestFailedException(String msg){
        super(msg);
        log.error("AuthRequestFailedException: " + this.getMessage());
    }
}
