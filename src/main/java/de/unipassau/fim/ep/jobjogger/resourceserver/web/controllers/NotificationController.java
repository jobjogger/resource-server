package de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.MarkNotificationDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.NotificationDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.Notification;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.NotificationService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

/**
 * This class provides controllers for handling notifications.
 */
@RestController
public class NotificationController {
    private final NotificationService notificationService;
    private final ModelMapper modelMapper;


    public NotificationController(NotificationService notificationService, ModelMapper modelMapper) {
        this.notificationService = notificationService;
        this.modelMapper = modelMapper;
    }

    /**
     * Get the notification history of a user.
     *
     * @param username is the user's id.
     * @param page     the page of the request.
     * @return a list of notifications.
     */
    @GetMapping("/inbox/{username}")
    @PreAuthorize("#username == authentication.name")
    public List<NotificationDto> getNotifications(@PathVariable @AuthenticationPrincipal String username,
                                                  @RequestParam(required = false, defaultValue = "1") int page) {
        Pageable pageable = PageRequest.of(page - 1, 10, Sort.by("timestamp"));
        return convertList(notificationService.getAllNotifications(username, pageable));
    }

    /**
     * Mark notification as viewed when the user has already seen them.
     *
     * @param username            is the user's id.
     * @param markNotificationDto contains the ids of the notifications.
     * @return 200 OK if it worked.
     */
    @PatchMapping("/inbox/{username}")
    @PreAuthorize("#username == authentication.name")
    public ResponseEntity<String> markAsViewed(@PathVariable String username,
                                               @RequestBody MarkNotificationDto markNotificationDto) {
        notificationService.markAsSeen(markNotificationDto.getIds(), username);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private List<NotificationDto> convertList(List<Notification> listJobs) {
        List<NotificationDto> result = new LinkedList<>();
        for (Notification notification : listJobs) {
            NotificationDto notifyDto = modelMapper.map(notification, NotificationDto.class);
            result.add(notifyDto);
        }
        return result;
    }
}
