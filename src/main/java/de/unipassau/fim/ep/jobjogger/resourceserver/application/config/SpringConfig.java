package de.unipassau.fim.ep.jobjogger.resourceserver.application.config;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.ImageMeta;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobMeta;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.ImageDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@EnableConfigurationProperties
@Configuration
public class SpringConfig {

    /**
     * Model-Mapper for transforming POJOs into DTOs.
     *
     * @return A Model-Mapper with right configuration.
     */
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        // JobDto should only reference the image with an ID to avoid JSON endless recursion.
        TypeMap<Job, JobDto> userTypeMap = modelMapper.createTypeMap(Job.class, JobDto.class);
        userTypeMap.setPostConverter(mappingContext -> {
            Job source = mappingContext.getSource();
            JobDto destination = mappingContext.getDestination();

            String dockerimg = source.getImage().getId();
            destination.setImageId(dockerimg);

            JobMeta jobMeta = new JobMeta(source.getCreateTime(), source.getPendingTime(), source.getStartTime(), source.getEndTime());
            destination.setMeta(jobMeta);
            return destination;
        });

        // ImageDto has custom metadata object for clearer structure.
        TypeMap<Image, ImageDto> imageTypeMap = modelMapper.createTypeMap(Image.class, ImageDto.class);
        imageTypeMap.setPostConverter(mappingContext -> {
            Image source = mappingContext.getSource();
            ImageDto destination = mappingContext.getDestination();
            ImageMeta imageMeta = modelMapper.map(source, ImageMeta.class);
            destination.setMeta(imageMeta);
            return destination;
        });
        return modelMapper;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /**
     * RestTemplate which is configured for transmitting larger files to the registration-service.
     * @return RestTemplate with correct settings.
     */
    @Bean
    @Qualifier(value = "fileTransRestTemplate")
    public RestTemplate fileTransRestTemplate(){
        RestTemplate restTemplate = new RestTemplate();
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setBufferRequestBody(false);
        requestFactory.setReadTimeout(120 * 1000);
        restTemplate.setRequestFactory(requestFactory);
        return restTemplate;
    }

}
