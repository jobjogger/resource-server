package de.unipassau.fim.ep.jobjogger.resourceserver.model.services;

import org.springframework.core.io.InputStreamResource;

import java.io.InputStream;

/**
 * Wrapper for the Multipart-file to perform POST on the registration server.
 */
public class MultipartInputStreamFileResource extends InputStreamResource {

    private final String filename;

    MultipartInputStreamFileResource(InputStream inputStream, String filename) {
        super(inputStream);
        this.filename = filename;
    }

    @Override
    public String getFilename() {
        return this.filename;
    }

    @Override
    public long contentLength() {
        return -1; // we do not want to generally read the whole stream into memory ...
    }
}