package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Map;

/**
 * Metadata that describes an image.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImageMeta {
    private Timestamp uploadTime;
    private Timestamp created;
    private String comment;
    private Collection<EnvVar> env;
    private Collection<String> cmd;
    private String workdir;
    private Collection<String> entrypoint;
    private Map<String,String> labels;
    private String dockerversion;
    private String author;
    private String architecture;
    private Integer size;
    private Integer virtualsize;
}
