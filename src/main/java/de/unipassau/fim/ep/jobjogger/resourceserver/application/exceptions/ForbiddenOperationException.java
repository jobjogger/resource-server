package de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Thrown if a user performs an illegal operation.
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
@Slf4j
public class ForbiddenOperationException extends RuntimeException{
    public ForbiddenOperationException(String msg){
        super(msg);
        log.error("ForbiddenOperationException: " + this.getMessage() );
    }
}
