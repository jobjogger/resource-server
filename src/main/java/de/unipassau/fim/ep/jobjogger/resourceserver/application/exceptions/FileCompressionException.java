package de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception is thrown when an error occurred during tar compression.
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
@Slf4j
public class FileCompressionException extends RuntimeException {
    public FileCompressionException(String msg) {
        super(msg);
        log.error("FileCompressionException: " + this.getMessage() );
    }
}
