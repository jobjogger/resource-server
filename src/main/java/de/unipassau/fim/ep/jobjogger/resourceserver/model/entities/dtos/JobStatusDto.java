package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A Job object that only contains its Id and its status.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobStatusDto {
    private Long id;
    private JobStatus status;
}
