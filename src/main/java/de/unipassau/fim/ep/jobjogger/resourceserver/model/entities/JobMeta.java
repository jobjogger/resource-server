package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * Metadata describing a job.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobMeta {
    private Timestamp createTime;
    private Timestamp pendingTime;
    private Timestamp startTime;
    private Timestamp endTime;
}
