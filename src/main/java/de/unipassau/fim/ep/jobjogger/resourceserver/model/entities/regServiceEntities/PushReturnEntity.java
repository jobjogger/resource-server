package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This DTO maps the response from the registration service and contains succeed and failed images that were (not)
 * pushed to the registration service.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PushReturnEntity {
    PushSuccessDto[] success;
    PushFailDto[] fail;

}
