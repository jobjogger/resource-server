package de.unipassau.fim.ep.jobjogger.resourceserver.model.repos;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for database-independent access to images.
 */
@Repository
public interface ImageRepo extends JpaRepository<Image, String> {
    Page<Image> findAllByUserIdOrderByUploadTimeDesc(String user, Pageable pageable);
}
