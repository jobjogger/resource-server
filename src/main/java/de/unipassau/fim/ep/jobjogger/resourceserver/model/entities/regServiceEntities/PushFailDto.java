package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This DTO maps the response from the registration service and contains a failed image that could not be pushed to the registry.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PushFailDto {
    String image;
    String user;
    String uri;
    String reason;
}
