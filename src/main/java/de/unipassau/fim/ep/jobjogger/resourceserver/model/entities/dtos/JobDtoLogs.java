package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.EnvVar;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobMeta;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Job DTO with log attribute to avoid sending big log files when requesting just basic job-information.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobDtoLogs {
    private Long id;
    @NotNull
    private String name;
    private String userId;
    @NotNull
    private String imageId;
    private JobStatus status;
    private List<EnvVar> environmentVariables;
    private String logs;
    private JobMeta meta;
}

