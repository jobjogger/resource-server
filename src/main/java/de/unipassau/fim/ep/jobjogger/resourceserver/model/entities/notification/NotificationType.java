package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification;

/**
 * The type of the notification describes the reason for notifying the user.
 */
public enum NotificationType {
    JOB_SUCCEEDED, JOB_FAILED, JOB_CANCELED
}
