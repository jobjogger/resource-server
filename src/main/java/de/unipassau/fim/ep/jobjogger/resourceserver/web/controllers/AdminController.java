package de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.ImageDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.DockerimageService;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.JobService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers.ControllerUtilityClass.*;

/**
 * This class contains all the controllers which the admin needs to perform his actions.
 */
@RestController
@PreAuthorize("hasRole('ADMIN')")
@Slf4j
public class AdminController {

    private final JobService jobService;
    private final DockerimageService dockerimageService;
    private final ModelMapper modelMapper;

    public AdminController(JobService jobService, ModelMapper modelMapper, DockerimageService dockerimageService) {
        this.jobService = jobService;
        this.modelMapper = modelMapper;
        this.dockerimageService = dockerimageService;
    }

    /**
     * Admin endpoint to get all jobs which are currently stored in the database.
     *
     * @param page Page number.
     * @param size Size of the page.
     * @param sort Sorted by attribute.
     * @return A list containing all jobs.
     */
    @GetMapping("/admins/jobs")
    public List<JobDto> getAllJobs(@RequestParam(required = false, defaultValue = "1") Integer page,
                                   @RequestParam(required = false, defaultValue = "10") Integer size,
                                   @RequestParam(required = false, defaultValue = "created") String sort) {
        Sort pageableSort = jobPageableSort(sort);
        Pageable pageable = PageRequest.of(page - 1, size, pageableSort);
        return convertJobList(modelMapper, jobService.getAllJobs(pageable));
    }

    /**
     * Controller for getting all jobs in the database having a certain status.
     *
     * @param status JobStatus object.
     * @param page   Page number.
     * @param size   Size of the page.
     * @param sort   Sorted by attribute.
     * @return List of jobs with certain status.
     */
    @GetMapping("/admins/jobs/status/{status}")
    public List<JobDto> getJobsByStatus(@PathVariable String status,
                                        @RequestParam(required = false, defaultValue = "1") Integer page,
                                        @RequestParam(required = false, defaultValue = "10") Integer size,
                                        @RequestParam(required = false, defaultValue = "created") String sort) {
        Sort pageableSort = jobPageableSort(sort);
        Pageable pageable = PageRequest.of(page - 1, size, pageableSort);
        return convertJobList(modelMapper, jobService.getAllJobsPerStatus(JobStatus.valueOf(status.toUpperCase()), pageable));
    }

    /**
     * Admin endpoint to get all images which are currently stored in the database.
     *
     * @param page The page.
     * @return A list containing all images.
     */
    @GetMapping("/admins/images")
    public List<ImageDto> getAllImages(@RequestParam(required = false, defaultValue = "1") Integer page,
                                       @RequestParam(required = false, defaultValue = "10") Integer size,
                                       @RequestParam(required = false, defaultValue = "upload") String sort) {
        Sort pageableSort = imagePageableSort(sort);
        Pageable pageable = PageRequest.of(page - 1, size, pageableSort);
        return convertImageList(modelMapper, dockerimageService.getAllImages(pageable));
    }

}
