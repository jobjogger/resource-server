package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This DTO maps the response from the registration service and contains an image that was pushed to the registry.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PushSuccessDto {
    String image;
    String user;
    String uri;
    FullImageMeta meta;
}
