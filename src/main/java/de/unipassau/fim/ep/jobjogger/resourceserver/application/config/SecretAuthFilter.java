package de.unipassau.fim.ep.jobjogger.resourceserver.application.config;


import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.http.HttpServletRequest;

/**
 * Filter that checks the http-header auth-field for the correct bearer-token in order to secure microservice communication.
 */
public class SecretAuthFilter extends AbstractPreAuthenticatedProcessingFilter {

    private final String principalRequestHeader;

    public SecretAuthFilter(String principalRequestHeader) {
        this.principalRequestHeader = principalRequestHeader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        return request.getHeader(principalRequestHeader);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        // Not necessary for our usage.
        return "N/A";
    }

}