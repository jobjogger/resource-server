package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos;


import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.EnvVar;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobMeta;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * JobDTO with attributes from a job that are transmitted to the user.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobDto {
    private Long id;
    @NotNull
    private String name;
    private String userId;
    @NotNull
    private String imageId;
    private JobStatus status;
    private List<EnvVar> environmentVariables;
    private JobMeta meta;
    private String priority;
    private String project;
    private boolean isCurrent;
}
