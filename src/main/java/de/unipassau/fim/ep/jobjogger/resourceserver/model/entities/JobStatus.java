package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities;

/**
 * A set of statuses a job can have during his lifecycle.
 */
public enum JobStatus {
    RUNNING, PENDING, STARTING, CANCELLING, FAILED, SUCCEEDED, CANCELED, NONE
}
