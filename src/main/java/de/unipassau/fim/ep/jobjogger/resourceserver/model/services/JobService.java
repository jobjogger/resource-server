package de.unipassau.fim.ep.jobjogger.resourceserver.model.services;

import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.*;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.EnvVar;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.NotificationType;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.repos.ImageRepo;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.repos.JobRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Service which communicates with the repositories to perform CRUD-operations concerning jobs.
 */
@Service
@Slf4j
public class JobService {

    @Value("${host.cluster-server}")
    private String clusterServerHost;

    private final RestTemplate restTemplate;

    private final JobRepo jobRepo;
    private final ImageRepo dockerRepo;
    private final NotificationService notificationService;

    public JobService(RestTemplate restTemplate, JobRepo jobRepo, ImageRepo dockerRepo, NotificationService notificationService1) {
        this.restTemplate = restTemplate;
        this.jobRepo = jobRepo;
        this.dockerRepo = dockerRepo;
        this.notificationService = notificationService1;
    }

    /**
     * Create a job from a dockerimage.
     *
     * @param name          is the name of the job.
     * @param userId        is the id of the user owning the image-
     * @param imageId is the id of the dockerimage which is being used.
     * @param priority      the priority of the pod
     * @return the created job.
     */
    public Job createJob(String name, String userId, String imageId, List<EnvVar> envVars, String priority) {
        Image image = dockerRepo.findById(imageId).orElseThrow(()->
                new EntityNotFoundException("Dockerimage with ID " + imageId + " not found"));

        if (!image.getUserId().equals(userId)) {
            throw new ForbiddenOperationException("User may not create a job from image he does not own");
        }
        Job job = jobRepo.save(new Job(name, userId, image, envVars, priority));
        log.info("Job with name'" + name + "' by  user '" + userId + "with priority" + priority + "' created.");

        return job;
    }

    /**
     * Sets the logs of an existing job.
     *
     * @param jobId  The ID of the job whose logs will be posted.
     * @param userId The ID of the user who owns the job.
     * @param logs   The logs of the job/pod.
     * @throws EntityNotFoundException     If the Job couldn't be found.
     * @throws ForbiddenOperationException If the user is not the owner of the Job.
     */
    public void postJobLogs(Long jobId, String userId, String logs) {
        Job job = getJobById(jobId);
        if (!job.getUserId().equals(userId)) {
            throw new ForbiddenOperationException("User not owning the job.");
        }
        job.setLogs(logs);
        log.info("Received logs from pod: " + job.getName());
        log.info("Logs: " + logs);
        jobRepo.save(job);
    }

    /**
     * Updates an existing job.
     *
     * @param jobId The ID of the Job to update.
     * @param data  The updated DTO of the Job.
     * @throws EntityInUseException    If the Job is currently PENDING or RUNNING.
     * @throws EntityNotFoundException If the Job can't be found.
     */
    public void updateJob(Long jobId, JobDto data) {
        Job job = getJobById(jobId);
        if (job.getJobStatus() == JobStatus.RUNNING || job.getJobStatus() == JobStatus.PENDING) {
            throw new EntityInUseException(("Job may not be updated because job is still running or scheduled."));
        }
        job.setName(data.getName());
        job.setEnvironmentVariables(data.getEnvironmentVariables());
        job.setPriority(data.getPriority());

        if (data.getProject() != null && !data.getProject().equals("")) {
            job.setProject(data.getProject());
        } else {
            job.setProject(null);
        }
        jobRepo.save(job);
        log.info("Job updated: " + job.getId());
    }


    /**
     * Updates the status of an existing job.
     *
     * @param jobId         The ID of the job that will be updated.
     * @param userId        The ID of the user who owns the job.
     * @param updatedStatus The new status of this job.
     * @throws EntityNotFoundException     If the Job was not found.
     * @throws ForbiddenOperationException If the User is not the Owner of the Job.
     */
    public void updateJobStatus(Long jobId, String userId, JobStatus updatedStatus) {
        Job job = getJobById(jobId);
        if (!job.getUserId().equals(userId)) {
            throw new ForbiddenOperationException("User does not own a job with this ID: " + jobId);
        }
        JobStatus prevJobStatus = job.getJobStatus();
        job.setJobStatus(updatedStatus);
        createNotification(prevJobStatus, job, userId);
        log.info("Updated Status from Job: " + userId + " " + jobId + ": " + job.getJobStatus().toString());

        jobRepo.save(job);
    }

    private void createNotification(JobStatus prevJobStatus, Job job, String userId) {
        NotificationType notificationType;
        JobStatus jobStatus = job.getJobStatus();
        switch (jobStatus) {
            case SUCCEEDED:
                notificationType = NotificationType.JOB_SUCCEEDED;
                break;
            case FAILED:
                if (!prevJobStatus.equals(JobStatus.CANCELED)) {
                    notificationType = NotificationType.JOB_FAILED;
                } else {
                    notificationType = null;
                }
                break;
            default:
                notificationType = null;
        }
        // Avoid spamming of notification when kubernetes events are triggered inflationary.
        if (notificationType != null && !jobStatus.equals(prevJobStatus)) {
            notificationService.createNotification(notificationType, userId, "", job);
            log.info("Notification created for job by " + userId);
        }
    }

    /**
     * Deletes an existing job.
     *
     * @param userId The ID of the user who wants to delete one of his jobs.
     * @param job    The Job that will be deleted.
     * @throws EntityInUseException        If the Job is currently RUNNING or PENDING.
     * @throws ForbiddenOperationException If the User is not the Owner of the Job.
     * @throws EntityNotFoundException     If the Job was not found.
     */
    public void deleteJob(String userId, Job job) {
        if (!jobRepo.existsById(job.getId())){
            throw new EntityNotFoundException("Job with id " + job.getId() + " not found.");
        }
        Long jobId = job.getId();
        if (job.getJobStatus() == JobStatus.RUNNING || job.getJobStatus() == JobStatus.PENDING) {
            throw new EntityInUseException("Job may not be deleted because job is still running or scheduled.");
        } else {
            jobRepo.delete(job);
            log.info("Job with name '" + jobId + "' by  user '" + userId + "' deleted.");
        }
    }

    /**
     * Returns all jobs that are stored in the database.
     *
     * @param pageable Pagination object.
     * @return List of jobs.
     */
    public List<Job> getAllJobs(Pageable pageable) {
        return iterableToList(jobRepo.findAll(pageable));
    }


    /**
     * Find all DB-entries with a certain job status.
     *
     * @param jobStatus Status signaling if a job is running.
     * @param pageable  Pagination object.
     * @return List of Jobs.
     */
    public List<Job> getAllJobsPerStatus(JobStatus jobStatus, Pageable pageable) {
        List<Job> jobList = iterableToList(jobRepo.findAllByJobStatusEquals(jobStatus, pageable));
        log.info("Return " + jobList.size() + " jobs with status " + jobStatus.toString());
        return jobList;
    }


    /**
     * Get all jobs by a certain user.
     *
     * @param userId   the id of the user.
     * @param pageable the page, size of the page and the sorting.
     * @return all jobs by a certain user.
     */
    public List<Job> getAllJobsByUser(String userId, Pageable pageable) {
        List<Job> jobList = iterableToList(jobRepo.findAllByUserId(userId, pageable));
        log.info("Return " + jobList.size() + " Jobs by user '" + userId + "'.");
        return jobList;
    }

    /**
     * Find all jobs with a certain job status by a specific user.
     *
     * @param jobStatus Status signaling if a job is running.
     * @param userId    ID
     * @param pageable  Pagination object.
     * @return List of Jobs
     */
    public List<Job> getUsersJobsPerStatus(JobStatus jobStatus, String userId, Pageable pageable) {
        List<Job> jobList = iterableToList(jobRepo.findAllByUserIdAndJobStatusEquals(userId, jobStatus, pageable));
        log.info("Return " + jobList.size() + " Jobs by user '" + userId + "' with status " + jobStatus.toString());
        return jobList;
    }

    /**
     * Find all Job statuses by job ids for a specific user.
     *
     * @param userId ID
     * @param jobIds Requested Job ids.
     * @return List of Jobs
     */
    public List<Job> getJobStatusesByIds(String userId, List<Long> jobIds) {
        log.info("Get all statuses from Jobs " + jobIds + " by user '" + userId + "'");
        return jobRepo.findAllByUserIdAndIdIn(userId, jobIds);
    }

    /**
     * Find all Job statuses by Job ids.
     *
     * @param jobIds Requested Job ids.
     * @return List of Jobs
     */
    public List<Job> getAllJobStatusesByIds(List<Long> jobIds) {
        log.info("Get all statuses from Jobs " + jobIds);
        return jobRepo.findAllByIdIn(jobIds);
    }

    /**
     * Gets the logs of a specific job.
     *
     * @param jobId The Id of the specific job.
     * @return The log of the job.
     * @throws EntityNotFoundException If the Job can't be found
     */
    public String getLogsOfJob(Long jobId, Integer lines) throws EntityNotFoundException {
        Job job = jobRepo.findById(jobId).orElse(null);
        if (job == null) {
            throw new EntityNotFoundException("Job with id '" + jobId + "'not found");
        }
        return getNumberOfLines(job.getLogs(), lines);
    }

    /**
     * Gets a job with a specific ID.
     *
     * @param id The Job ID.
     * @return The Job with the ID.
     */
    public Job getJobById(Long id) {
        Job job = jobRepo.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Job with id " + id + " not found."));
        log.info("Return job with id '" + id + "'.");
        return job;
    }

    private static List<Job> iterableToList(Iterable<Job> it) {
        List<Job> list = new LinkedList<>();
        if (it == null){
            return list;
        }
        for (Job job : it) {
            list.add(job);
        }
        return list;
    }

    /**
     * Creates a POST request from this server to the cluster-service server when creating a new job/pod.
     *
     * @param username The namespace of the job/pod.
     * @param job      DTO of the Job.
     */
    public ResponseEntity<String> clusterStartRequest(String username, JobDto job) {
        //Set that the job is based on a current Image if it wasn't previously
        Job dbJob = jobRepo.findById(job.getId()).orElse(null);
        if (dbJob != null) {
            dbJob.setCurrent(true);
            jobRepo.save(dbJob);
        }

        updateJobStatus(job.getId(), username, JobStatus.STARTING);
        String clusterServiceUrl = clusterServerHost + "/" + username + "/" + job.getId();
        log.info("posting Job to " + clusterServiceUrl);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<JobDto> request = new HttpEntity<>(job, httpHeaders);
        ResponseEntity<String> response;
        try {
            response = restTemplate.postForEntity(clusterServiceUrl, request, String.class);
        } catch (HttpStatusCodeException exc) {
            response = new ResponseEntity<>(exc.getResponseBodyAsString(), exc.getStatusCode());
            if (response.getStatusCode() == HttpStatus.CONFLICT) {
                updateJobStatus(job.getId(), username, JobStatus.RUNNING);
                throw new JobAlreadyRunningException("The Job " + username + "/" + job.getId() + " is already running.");
            }
            updateJobStatus(job.getId(), username, JobStatus.FAILED);
        }
        return response;
    }

    /**
     * Creates a DELETE request from this server to the cluster-service server to delete a pod
     * in the cluster-service when canceling a job.
     *
     * @param username The namespace of the job/pod.
     * @param job      DTO of the Job.
     */
    public ResponseEntity<String> clusterDeleteRequest(String username, JobDto job) {
        updateJobStatus(job.getId(), username, JobStatus.CANCELLING);
        String clusterServiceUrl = clusterServerHost + "/" + username + "/" + job.getId();
        log.info("Delete Request to " + clusterServiceUrl);

        ResponseEntity<String> response;
        try {
            response = restTemplate.exchange(clusterServiceUrl, HttpMethod.DELETE, null, String.class);
        } catch (HttpStatusCodeException exc) {
            throw new CancelJobOperationException("Canceling the Job" + username + "/" + job.getId() + "was not possible." +
                    "Status-Code: " + exc.getStatusCode() + ". Message: " + exc.getMessage());
        }

        return response;
    }

    private static String getNumberOfLines(String data, Integer linesNum) {
        if (data == null){
            return "";
        }
        // Split by new line.
        String[] lines = data.split("\r\n|\r|\n");

        // If more lines are requested than exists the whole array String will be returned.
        if (lines.length <= linesNum || linesNum.equals(0)) {
            return data;
        }
        String[] linesArr = Arrays.copyOfRange(lines, lines.length - linesNum, lines.length);
        StringBuilder export = new StringBuilder();
        for (String s : linesArr) {
            export.append(s);
            export.append(System.getProperty("line.separator"));
        }
        return export.toString();
    }
}
