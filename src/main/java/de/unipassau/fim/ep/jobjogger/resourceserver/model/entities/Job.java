package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A job is a calculation derived from an image.
 */
@Entity
@Data
@NoArgsConstructor
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String userId;

    @ManyToOne
    @JoinColumn
    private Image image;

    private JobStatus jobStatus;

    @NotNull
    private String priority;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private String logs;

    @ElementCollection
    @CollectionTable
    private Collection<EnvVar> environmentVariables;

    @Column
    @CreationTimestamp
    private Timestamp createTime;

    private Timestamp pendingTime;

    private Timestamp startTime;

    private Timestamp endTime;

    private String project;

    private boolean isCurrent = true;

    public Job(@NotNull String name, @NotNull String userId, Image image) {
        this.name = name;
        this.userId = userId;
        this.image = image;
        this.setJobStatus(JobStatus.NONE);
        this.environmentVariables = new ArrayList<>();
    }

    public Job(@NotNull String name, @NotNull String userId, Image image, List<EnvVar> envVars) {
        this.name = name;
        this.userId = userId;
        this.image = image;
        this.setJobStatus(JobStatus.NONE);
        this.environmentVariables = envVars;
    }

    public Job(@NotNull String name, @NotNull String userId, Image image, List<EnvVar> envVars, String priority) {
        this.name = name;
        this.userId = userId;
        this.image = image;
        this.priority = priority;
        this.jobStatus = JobStatus.NONE;
        this.environmentVariables = envVars;
    }


    public void setJobStatus(JobStatus jobStatus) {
        this.jobStatus = jobStatus;
        setStatusTime();
    }

    private void setStatusTime() {
        Timestamp current = new Timestamp(System.currentTimeMillis());
        switch (jobStatus) {
            case NONE:
                setCreateTime(current);
            case PENDING:
                setPendingTime(current);
                break;
            case RUNNING:
                setStartTime(current);
                break;
            case SUCCEEDED:
            case FAILED:
            case CANCELED:
                setEndTime(current);
                break;
            default:
                break;
        }
    }
}
