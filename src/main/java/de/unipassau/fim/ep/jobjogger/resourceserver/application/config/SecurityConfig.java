package de.unipassau.fim.ep.jobjogger.resourceserver.application.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * Configuration for the WebSecurity instance in order to secure internal server communication.
 */
@Configuration
@EnableResourceServer
@EnableWebSecurity
@Slf4j
@Order(1)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${microservice-secret.header-name}")
    private String principalRequestHeader;

    @Value("${microservice-secret.secret-token}")
    private String principalRequestValue;

    /**
     * Overrides WebSecurity to ignore requests
     *
     * @param web current web security
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        SecretAuthFilter filter = new SecretAuthFilter(principalRequestHeader);
        principalRequestValue = "Bearer " + principalRequestValue;
        filter.setAuthenticationManager(authentication -> {
            String principal = authentication.getPrincipal().toString();
            if (!principalRequestValue.equals(principal)) {
                log.error("Wrong microservice secret in request... " +
                        "VALUE: " + principalRequestValue + ", Principal: " + principal);
                throw new BadCredentialsException("The API key was not found or not the expected value.");
            }
            authentication.setAuthenticated(true);
            return authentication;
        });
        http.
                antMatcher("/cluster/**").
                csrf().disable().
                sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
                and().addFilter(filter).authorizeRequests().anyRequest().authenticated();
    }

}
