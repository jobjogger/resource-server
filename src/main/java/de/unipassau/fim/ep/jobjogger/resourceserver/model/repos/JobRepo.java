package de.unipassau.fim.ep.jobjogger.resourceserver.model.repos;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for database-independent access to jobs.
 */
@Repository
public interface JobRepo extends JpaRepository<Job, Long> {
    Page<Job> findAllByUserId(String id, Pageable pageable);

    Page<Job> findAllByUserIdAndJobStatusEquals(String userId, JobStatus jobStatus, Pageable pageable);

    Page<Job> findAllByJobStatusEquals(JobStatus jobStatus, Pageable pageable);

    List<Job> findAllByUserIdAndIdIn(String userId, List<Long> jobIds);

    List<Job> findAllByIdIn(List<Long> jobIds);
}
