package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Notification object which is created if an important event occurred and the user should be informed about.
 */
@Entity
@Data
@NoArgsConstructor
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private NotificationType notificationType;

    @CreationTimestamp
    private Timestamp timestamp;

    @NotNull
    private String userId;

    private boolean isSeen = false;

    private Long jobId;

    private String jobName;

    private String imageName;

    private String reason;

    public Notification(NotificationType notificationType, String userId, String reason, Long jobId, String jobName,
                        String imageName){
        this.notificationType = notificationType;
        this.userId = userId;
        this.reason = reason;
        this.jobId = jobId;
        this.jobName = jobName;
        this.imageName = imageName;
    }
}
