package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.ImageMeta;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Dto for Returning to the client. The file variable is being omitted as it would be too big.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ImageDto {
    private String id;
    private String name;
    private String project;
    private List<JobDto> jobs;
    private String userId;
    private boolean hasSourceArchive;
    private ImageMeta meta;
}
