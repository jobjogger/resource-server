package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Text file containing code and was created by a user.
 */
@Data
@AllArgsConstructor
public class TextFileDto {
    private String name;
    private String content;
}
