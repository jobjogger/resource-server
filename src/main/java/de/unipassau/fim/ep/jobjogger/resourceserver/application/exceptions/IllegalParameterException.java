package de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Thrown when wrong usage of parameters occurred.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
@Slf4j
public class IllegalParameterException extends RuntimeException {
    public IllegalParameterException(String msg) {
        super(msg);
        log.error("IllegalParameterException: " + this.getMessage() );
    }
}
