package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.FullImageMeta;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * A image is uploaded by a user and can be used to create jobs and starting a calculation.
 */
@Entity
@NoArgsConstructor
@Data
public class Image {
    @Id
    String id;

    @Column
    @CreationTimestamp
    private Timestamp uploadTime;

    @NotNull
    private String name;

    private String project;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "image", orphanRemoval = true, cascade = CascadeType.REMOVE)
    @JsonBackReference
    private List<Job> jobs;

    @NotNull
    private String userId;

    private Timestamp created;
    private String comment;

    @ElementCollection
    @CollectionTable
    private Collection<EnvVar> env;

    @ElementCollection
    private  Collection<String> cmd;
    private String workdir;
    @ElementCollection
    private  Collection<String> entrypoint;

    @ElementCollection
    @CollectionTable
    private Map<String, String> labels;
    private  String dockerversion;
    private String author;
    private  String architecture;
    private  Integer size;
    private  Integer virtualsize;
    private Boolean hasSourceArchive;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] sourceArchive;



    public Image(String userId, String id){
        this.id = id;
        this.name = defaultImageName(id);
        this.userId = userId;
        this.jobs = new LinkedList<>();
    }

    public void setSourceArchive(byte[] sourceArchive){
        hasSourceArchive = true;
        this.sourceArchive = sourceArchive;
    }

    private String defaultImageName(String id){
        return id.substring(id.indexOf("/")+1);
    }

    public void injectMetadata(FullImageMeta meta){
        created = meta.getCreated();
        comment = meta.getComment();
        env = meta.getEnv();
        cmd = meta.getCmd();
        workdir = meta.getWorkdir();
        entrypoint = meta.getEntrypoint();
        labels = meta.getLabels();
        dockerversion = meta.getDockerversion();
        author = meta.getAuthor();
        architecture = meta.getArchitecture();
        size = meta.getSize();
        virtualsize = meta.getVirtualsize();
    }
}
