package de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.ImageDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDto;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;

import java.util.LinkedList;
import java.util.List;

public class ControllerUtilityClass {

    static Sort jobPageableSort(String sort) {
        Sort pageableSort;
        switch (sort) {
            case "name":
                pageableSort = Sort.by("name", "jobStatus");
                break;
            case "status":
                pageableSort = Sort.by("jobStatus", "id");
                break;
            case "project":
                pageableSort = Sort.by("project").and(Sort.by("createTime").descending());
                break;
            case "created":
            default:
                pageableSort = Sort.by("createTime").descending().and(Sort.by("jobStatus"));
                break;
        }
        return pageableSort;
    }

    static Sort imagePageableSort(String sort) {
        Sort pageableSort;
        switch (sort) {
            case "name":
                pageableSort = Sort.by("name");
                break;
            case "user":
                pageableSort = Sort.by("userId").and(Sort.by("uploadTime").descending());
                break;
            case "project":
                pageableSort = Sort.by("project").and(Sort.by("uploadTime").descending());
                break;
            case "created":
            default:
                pageableSort = Sort.by("uploadTime").descending();

                break;
        }
        return pageableSort;
    }

    static List<JobDto> convertJobList(ModelMapper modelMapper, List<Job> listJobs) {
        List<JobDto> result = new LinkedList<>();
        for (Job job : listJobs) {
            JobDto jobDto = modelMapper.map(job, JobDto.class);
            result.add(jobDto);
        }
        return result;
    }

    static List<ImageDto> convertImageList(ModelMapper modelMapper, List<Image> images) {
        List<ImageDto> result = new LinkedList<>();

        for (Image image : images) {
            result.add(modelMapper.map(image, ImageDto.class));
        }

        return result;
    }
}
