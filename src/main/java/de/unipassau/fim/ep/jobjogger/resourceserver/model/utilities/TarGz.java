package de.unipassau.fim.ep.jobjogger.resourceserver.model.utilities;

import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.FileCompressionException;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.TextFileDto;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;

import java.io.*;
import java.util.ArrayList;

public class TarGz {

    /**
     * Utility for compressing a collection of text files to a ByteArrayInputStream.
     *
     * @param files the text files which should be compressed.
     * @return the compressed archive.
     */
    public static ByteArrayOutputStream compress(TextFileDto[] files) {
        return gzip(new ByteArrayInputStream(tar(files).toByteArray()));
    }

    /**
     * Utility for decompressing a collection of text files from a ByteArrayInputStream.
     *
     * @param input The Archive to decompress as a ByteArrayInputStream.
     * @return An Array of TextFileDtos.
     */
    public static TextFileDto[] decompress(ByteArrayInputStream input) {
        return untar(new ByteArrayInputStream(gunzip(input).toByteArray()));
    }

    private static ByteArrayOutputStream tar(TextFileDto[] files) {
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            TarArchiveOutputStream tarOut = new TarArchiveOutputStream(bout);
            for (TextFileDto file : files) {
                TarArchiveEntry entry = new TarArchiveEntry(file.getName());
                entry.setSize(file.getContent().getBytes().length);
                tarOut.putArchiveEntry(entry);
                tarOut.write(file.getContent().getBytes());
                tarOut.closeArchiveEntry();
            }
            tarOut.close();
            return bout;
        } catch (IOException e) {
            e.printStackTrace();
            throw new FileCompressionException(e.getMessage());
        }
    }

    private static TextFileDto[] untar(InputStream in) {
        try {
            TarArchiveInputStream tarInput = new TarArchiveInputStream(in);
            TarArchiveEntry entry;
            ArrayList<TextFileDto> files = new ArrayList<>();
            while ((entry = tarInput.getNextTarEntry()) != null) {
                int size = (int) entry.getSize();
                byte[] content = new byte[size];
                tarInput.read(content, 0, size);
                files.add(new TextFileDto(entry.getName(), new String(content)));
            }
            return files.toArray(new TextFileDto[0]);
        } catch (IOException e) {
            e.printStackTrace();
            throw new FileCompressionException(e.getMessage());
        }
    }

    private static ByteArrayOutputStream gzip(InputStream in) {
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            BufferedOutputStream out = new BufferedOutputStream(bout);
            GzipCompressorOutputStream gzOut = new GzipCompressorOutputStream(out);
            final byte[] buffer = new byte[64];
            int n;
            while (-1 != (n = in.read(buffer))) {
                gzOut.write(buffer, 0, n);
            }
            gzOut.close();
            return bout;
        } catch (IOException e) {
            e.printStackTrace();
            throw new FileCompressionException(e.getMessage());
        }
    }

    private static ByteArrayOutputStream gunzip(InputStream in) {
        try {
            BufferedInputStream bin = new BufferedInputStream(in);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            GzipCompressorInputStream gzIn = new GzipCompressorInputStream(bin);
            final byte[] buffer = new byte[64];
            int n;
            while (-1 != (n = gzIn.read(buffer))) {
                out.write(buffer, 0, n);
            }
            out.close();
            gzIn.close();
            return out;
        } catch (IOException e) {
            e.printStackTrace();
            throw new FileCompressionException(e.getMessage());
        }
    }
}
