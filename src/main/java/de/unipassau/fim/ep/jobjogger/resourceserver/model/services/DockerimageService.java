package de.unipassau.fim.ep.jobjogger.resourceserver.model.services;

import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.*;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Image;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.ImageDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.TextFileDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.PushReturnEntity;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.regServiceEntities.PushSuccessDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.repos.ImageRepo;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.repos.JobRepo;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.utilities.TarGz;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * This Service communicates with the repositories to perform CRUD-operations concerning images.
 */
@Service
@Slf4j
public class DockerimageService {

    private final ImageRepo imageRepo;

    private final JobRepo jobRepo;

    private final RestTemplate restTemplate;

    @Qualifier(value = "fileTransRestTemplate")
    private final RestTemplate fileTransRestTemplate;

    @Value("${host.registration-server}")
    private String registrationServerHost;

    public DockerimageService(ImageRepo dockerRepo, JobRepo jobRepo, RestTemplate restTemplate, RestTemplate fileTransRestTemplate) {
        this.imageRepo = dockerRepo;
        this.jobRepo = jobRepo;
        this.restTemplate = restTemplate;
        this.fileTransRestTemplate = fileTransRestTemplate;
    }

    /**
     * Creates a image and inserts it into the database.
     *
     * @param file   Binary file which is the Dockerimage.
     * @param userId The Id of the User who owns the Dockerimage.
     * @return The Dockerimage that was created.
     */
    public PushReturnEntity createDockerimage(MultipartFile file, String userId) {
        MultipartInputStreamFileResource multiPResource;
        try {
            multiPResource = new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename());
        } catch (IOException e) {
            throw new IllegalParameterException("Could not read input-stream of file.");
        }
        PushReturnEntity pushReturnEntity = postToRegService(multiPResource, "/" + userId);
        log.info("Response from reg-service: " + pushReturnEntity.toString());
        persistRegisteredDockerimage(pushReturnEntity, userId);
        return pushReturnEntity;
    }

    /**
     * Create image by input stream and not by multipart file.
     *
     * @param inputStream the input stream of the resource.
     * @param userId the user owning the image.
     * @return the response of the registration service.
     */
    public PushReturnEntity createDockerimage(InputStream inputStream, String userId) {
        MultipartInputStreamFileResource multiPResource;
        multiPResource = new MultipartInputStreamFileResource(inputStream, "test_file");
        PushReturnEntity pushReturnEntity = postToRegService(multiPResource, "/" + userId);
        log.info("Response from reg-service: " + pushReturnEntity.toString());
        persistRegisteredDockerimage(pushReturnEntity, userId);
        return pushReturnEntity;
    }

    /**
     * Builds a Dockerimage from a collection of text files.
     *
     * @param files     Text files to build the image from.
     * @param userId    The Id of the User who owns the Dockerimage.
     * @param imageId The desired name for the Dockerimage.
     * @return the Dockerimage that was created.
     */
    public PushReturnEntity buildDockerImage(TextFileDto[] files, String userId, String imageId) {
        MultipartInputStreamFileResource multiPResource;
        byte[] bytes = TarGz.compress(files).toByteArray();
        InputStream input = new ByteArrayInputStream(bytes);
        multiPResource = new MultipartInputStreamFileResource(input, "archive.tar.gz");
        PushReturnEntity pushReturnEntity = postToRegService(multiPResource, "/build/" + imageId);
        persistRegisteredDockerimage(pushReturnEntity, userId, bytes);
        return pushReturnEntity;

    }

    private void persistRegisteredDockerimage(PushReturnEntity pushReturnEntity, String userId, byte[] bytes) {
        for (PushSuccessDto pushSuccessDto : pushReturnEntity.getSuccess()) {
            String username = pushSuccessDto.getUser();
            if (username.equals(userId)) {
                Image image = new Image(userId, pushSuccessDto.getImage());
                image.setSourceArchive(bytes);
                image.injectMetadata(pushSuccessDto.getMeta());
                markJobsAsOutdated(image.getId());
                Image saved = imageRepo.save(image);
                log.info("Dockerimage with name '" + saved.getName() + "' by user '" + userId + "' saved.");
            } else {
                throw new ForbiddenOperationException("Upload-user and username pushed to reg-service do not match.");
            }
        }
    }

    private void persistRegisteredDockerimage(PushReturnEntity pushReturnEntity, String userId) {
        for (PushSuccessDto pushSuccessDto : pushReturnEntity.getSuccess()) {
            String username = pushSuccessDto.getUser();
            if (username.equals(userId)) {
                Image image = new Image(userId, pushSuccessDto.getImage());
                image.injectMetadata(pushSuccessDto.getMeta());
                markJobsAsOutdated(image.getId());
                Image saved = imageRepo.save(image);
                log.info("Dockerimage with name '" + saved.getName() + "' by user '" + userId + "' saved.");
            } else {
                throw new ForbiddenOperationException("Upload-user and username pushed to reg-service do not match.");
            }
        }
    }

    private void markJobsAsOutdated(String imageId) {
        Image existingImage = imageRepo.findById(imageId).orElse(null);
        if(existingImage != null) {
            for (Job job : existingImage.getJobs()) {
                if(job.getJobStatus() != JobStatus.NONE) {
                    job.setCurrent(false);
                    jobRepo.save(job);
                }
            }
        }
    }

    /**
     * Forward the image file to the registration server which loads the file and pushes it to the docker-registry.
     *
     * @param multiPResource input stream of the file to be sent.
     * @param route          destination at registration service.
     * @return the response of the registration service.e
     */
    private PushReturnEntity postToRegService(MultipartInputStreamFileResource multiPResource, String route) {
        HttpEntity<InputStreamResource> requestEntity = new HttpEntity<>(multiPResource);
        String url = registrationServerHost + route;
        ResponseEntity<PushReturnEntity> entity;
        try {
         entity = fileTransRestTemplate.exchange(url, HttpMethod.POST, requestEntity, PushReturnEntity.class);
        } catch (HttpStatusCodeException exc){
            throw new ForwardFileException("Could not pass file to docker-registry. " +
                    "Status code: " + exc.getStatusCode() + ". Is your code working?");
        }
        log.info("Successfully pushed to registration service.");
        return entity.getBody();

    }

    /**
     * Reads one image by its IDd from the database.
     *
     * @param id The Id of the image.
     * @return The image that was requested.
     */
    public Image readOneDockerimageById(String id) {
        Image found = imageRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Dockerimage with id '" + id + "' was not found."));
        log.info("Found dockerimage with id '" + id + "'.");
        return found;
    }

    /**
     * Reads all images owned by a user from the database.
     *
     * @param userId   The User who owns the image.
     * @param pageable The Pageable object for pagination and sorting.
     * @return All Dockerimages owned by a user.
     */
    public List<Image> readAllDockerimagesByUser(String userId, Pageable pageable) {
        List<Image> imageList =
                iterableToList(imageRepo.findAllByUserIdOrderByUploadTimeDesc(userId, pageable));
        log.info("Found " + imageList.size() + " dockerimages by user '" + userId + "'.");
        return imageList;
    }

    /**
     * Updates a Dockerimage by its Id in the database.
     *
     * @param id   The Id of the Dockerimage.
     * @param data The new data of the Dockerimage.
     */
    public void updateDockerimage(String id, ImageDto data) {
        if (data == null){
            return;
        }
        Image image = readOneDockerimageById(id);
        image.setName(data.getName());
        image.setProject(data.getProject());
        imageRepo.save(image);
        log.info("Dockerimage with id '" + id + "' updated.");
    }

    /**
     * Deletes a image by its Id in the database.
     * <p>
     * Blocks deleting when a SCHEDULED or RUNNING job derived from the Dockerimage.
     *
     * @param id The Id of the Dockerimage.
     */
    public void deleteDockerimage(String id) {
        Image image = readOneDockerimageById(id);
        if (image.getJobs().stream()
                .anyMatch(job -> job.getJobStatus() == JobStatus.RUNNING || job.getJobStatus() == JobStatus.PENDING)) {
            throw new EntityInUseException("At least one job derived from this image are RUNNING or SCHEDULED.");
        }
        try {
            restTemplate.exchange(registrationServerHost + "/" + id, HttpMethod.DELETE, null, String.class);
        } catch (HttpStatusCodeException exc) {
            if (exc.getStatusCode().is4xxClientError()) {
                throw new EntityNotFoundException(exc.getResponseBodyAsString());
            }
            log.error("Deleting image request went wrong. Status-code: " + exc.getStatusCode() + "\n" +
                    "Body: " + exc.getResponseBodyAsString());
        }
        imageRepo.delete(image);
        log.info("Dockerimage with id '" + id + "' deleted.");
    }

    private static List<Image> iterableToList(Iterable<Image> it) {
        List<Image> list = new LinkedList<>();
        if (it == null){
            it = list;
        }
        for (Image job : it) {
            list.add(job);
        }
        return list;
    }

    /**
     * Get SourceArchive from database, decompress it and return it.
     *
     * @param id of the image.
     * @return the uncompressed files.
     */
    public TextFileDto[] getSourceArchive(String id) {
        Image found = readOneDockerimageById(id);
        log.info("Found image with id '" + id + "'.");
        return TarGz.decompress(new ByteArrayInputStream(found.getSourceArchive()));
    }

    /**
     * Gets all Images, stored in the Database.
     *
     * @return All Images.
     */
    public List<Image> getAllImages(Pageable pageable) {
        return iterableToList(imageRepo.findAll(pageable));
    }
}
