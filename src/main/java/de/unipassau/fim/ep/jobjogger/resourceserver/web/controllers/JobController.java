package de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers;


import de.unipassau.fim.ep.jobjogger.resourceserver.application.exceptions.ForbiddenOperationException;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.EnvVar;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.Job;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.JobStatus;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.CancelJobDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos.JobStatusDto;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.NotificationType;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.JobService;
import de.unipassau.fim.ep.jobjogger.resourceserver.model.services.NotificationService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers.ControllerUtilityClass.convertJobList;
import static de.unipassau.fim.ep.jobjogger.resourceserver.web.controllers.ControllerUtilityClass.jobPageableSort;


/**
 * This class provides controllers for job management.
 */
@RestController
public class JobController {
    private final JobService jobService;
    private final ModelMapper modelMapper;
    private final NotificationService notificationService;

    private static final String DEFAULTPRIORITY = "default";

    public JobController(JobService jobService, ModelMapper modelMapper, NotificationService notificationService) {
        this.jobService = jobService;
        this.modelMapper = modelMapper;
        this.notificationService = notificationService;
    }

    /**
     * Create new Job controller. Does not start it right away.
     *
     * @param username user identifier.
     * @param job      job object to persist in the database.
     * @return the newly created job.
     */
    @PreAuthorize("#username == authentication.name or hasRole('ADMIN')")
    @PostMapping("/users/{username}/jobs")
    public JobDto postNewJob(@PathVariable("username") @AuthenticationPrincipal String username,
                             @RequestBody JobDto job) {
        List<EnvVar> envVars = job.getEnvironmentVariables();
        String priority = DEFAULTPRIORITY;
        if (isCurrentUserAdmin()) {
            priority = job.getPriority();
        }
        Job newJob = jobService.createJob(job.getName(), username, job.getImageId(), envVars, priority);
        return modelMapper.map(newJob, JobDto.class);
    }

    /**
     * Start job with certain id.
     *
     * @param username user identifier.
     * @param jobId    id of the job that already exists in the db.
     * @return 200 OK if it worked.
     */
    @PreAuthorize("#username == authentication.name")
    @PatchMapping("/users/{username}/jobs/{jobId}/start")
    public ResponseEntity<String> startJob(@PathVariable("username") @AuthenticationPrincipal String username,
                                           @PathVariable Long jobId) {
        JobDto jobDto = modelMapper.map(jobService.getJobById(jobId), JobDto.class);
        return jobService.clusterStartRequest(username, jobDto);
    }

    /**
     * Controller for getting all Jobs by a User.
     *
     * @param username The user who owns the jobs.
     * @param page     Number of pages indexed by 1.
     * @param size     Number of entries per page.
     * @param sort     Sorting of entries.
     * @return A list that contains all jobs by a user, sorted by status and creation time.
     */
    @PreAuthorize("#username == authentication.name or hasRole('ADMIN')")
    @GetMapping("/users/{username}/jobs")
    public List<JobDto> getAllJobsByUser(@PathVariable("username") @AuthenticationPrincipal String username,
                                         @RequestParam(required = false, defaultValue = "1") Integer page,
                                         @RequestParam(required = false, defaultValue = "10") Integer size,
                                         @RequestParam(required = false, defaultValue = "created") String sort) {
        Sort pageableSort = jobPageableSort(sort);
        Pageable pageable = PageRequest.of(page - 1, size, pageableSort);

        return convertJobList(modelMapper, jobService.getAllJobsByUser(username, pageable));
    }

    /**
     * Controller for getting one Job of a user.
     *
     * @param username The user who owns the job.
     * @param id       The job-ID.
     * @return The job object with the job-Id in JSON format.
     */
    @PreAuthorize("#username == authentication.name or hasRole('ADMIN')")
    @GetMapping("/users/{username}/jobs/{id:\\d+}")
    public JobDto getOneJobByUser(@PathVariable("username") @AuthenticationPrincipal String username,
                                  @PathVariable Long id) {
        return modelMapper.map(jobService.getJobById(id), JobDto.class);
    }

    /**
     * Controller for canceling a job.
     *
     * @param username The user who owns the job.
     * @param id       The job-ID.
     * @return 200 OK HTTP Status-Code.
     */
    @PreAuthorize(("#username == authentication.name or hasRole('ADMIN')"))
    @PatchMapping("/users/{username}/jobs/{id}/cancel")
    public ResponseEntity<String> cancelJob(@PathVariable("username") @AuthenticationPrincipal String username,
                                            @PathVariable Long id,
                                            @RequestBody(required = false) CancelJobDto cancelJobDto) {
        Job job = jobService.getJobById(id);
        JobDto jobDto = modelMapper.map(job, JobDto.class);

        ResponseEntity<String> responseEntity;
        try {
            responseEntity = jobService.clusterDeleteRequest(jobDto.getUserId(), jobDto);
        }catch (HttpStatusCodeException exc){
            return new ResponseEntity<>(exc.getResponseBodyAsString(), exc.getStatusCode());
        }
        if (isCurrentUserAdmin() && !username.equals(jobDto.getUserId()) && responseEntity.getStatusCode().is2xxSuccessful()) {
            String reason = "";
            if (cancelJobDto != null) {
                reason = cancelJobDto.getReason();
            }
            notificationService.createNotification(NotificationType.JOB_CANCELED, jobDto.getUserId(), reason, job);
        }
        return responseEntity;
    }

    /**
     * Controller for getting Jobs statuses by Ids.
     *
     * @param username The user who owns the jobs.
     * @param ids      The ids, separated by commas.
     * @return A list of all statuses of the requested Jobs.
     */
    @PreAuthorize("#username == authentication.name or hasRole('ADMIN')")
    @GetMapping("/users/{username}/jobs/statuses")
    public List<JobStatusDto> getJobsStatusesByIds(@PathVariable @AuthenticationPrincipal String username,
                                                   @RequestParam String ids) {
        List<Long> jobIds = Arrays.stream(ids.split(",")).map(Long::parseLong).collect(Collectors.toList());
        List<Job> jobs;
        if (isCurrentUserAdmin()) {
            jobs = jobService.getAllJobStatusesByIds(jobIds);
        } else {
            jobs = jobService.getJobStatusesByIds(username, jobIds);
        }

        return jobs.stream().map(job -> modelMapper.map(job, JobStatusDto.class)).collect(Collectors.toList());
    }

    /**
     * Controller for getting Jobs of a user by status.
     *
     * @param username The user who owns the jobs.
     * @param page     Number of pages indexed by 1.
     * @param size     Number of entries per page.
     * @param sort     Sorting of entries.
     * @return A list of all running jobs.
     */
    @PreAuthorize("#username == authentication.name or hasRole('ADMIN')")
    @GetMapping("/users/{username}/jobs/status/{status}")
    public List<JobDto> getJobsOfUserByStatus(@PathVariable @AuthenticationPrincipal String username,
                                              @PathVariable String status,
                                              @RequestParam(required = false, defaultValue = "1") Integer page,
                                              @RequestParam(required = false, defaultValue = "10") Integer size,
                                              @RequestParam(required = false, defaultValue = "created") String sort) {
        Sort pageableSort = jobPageableSort(sort);
        Pageable pageable = PageRequest.of(page - 1, size, pageableSort);

        return convertJobList(modelMapper, jobService.getUsersJobsPerStatus(JobStatus.valueOf(status.toUpperCase()), username, pageable));
    }


    /**
     * Controller for getting logs of a specific job.
     *
     * @param username The user who owns the job.
     * @param jobId    The Job-ID.
     * @return 200 OK HTTP Status-Code.
     */
    @PreAuthorize(("#username == authentication.name or hasRole('ADMIN')"))
    @GetMapping("/users/{username}/jobs/{jobId}/logs")
    public ResponseEntity<String> getLogsOfJob(@PathVariable @AuthenticationPrincipal String username,
                                               @PathVariable Long jobId,
                                               @RequestParam(required = false, defaultValue = "0") String lines) {
        Integer linesInt = Integer.valueOf(lines);
        String logs = jobService.getLogsOfJob(jobId, linesInt);
        return new ResponseEntity<>(logs, HttpStatus.OK);
    }

    /**
     * Controller for updating a job.
     *
     * @param username The user who owns the job.
     * @param id       The Job-ID.
     * @param data     The Job Data Object.
     * @return 200 OK HTTP Status-Code.
     */
    @PreAuthorize("#username == authentication.name")
    @PutMapping("/users/{username}/jobs/{id}")
    public ResponseEntity<String> updateJob(@PathVariable @AuthenticationPrincipal String username, @PathVariable Long id,
                                            @NotNull @RequestBody JobDto data) {
        if (!isCurrentUserAdmin()){
            data.setPriority(DEFAULTPRIORITY);
        }
        jobService.updateJob(id, data);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Delete a job if it is not RUNNING or PENDING.
     *
     * @param username User owning the job.
     * @param id       Job-ID.
     * @return 200 OK HTTP Status-Code.
     */
    @PreAuthorize("#username == authentication.name or hasRole('ADMIN')")
    @DeleteMapping("/users/{username}/jobs/{id}")
    public ResponseEntity<String> deleteExistingJob(@PathVariable @AuthenticationPrincipal String username,
                                                    @PathVariable Long id) {
        Job job = jobService.getJobById(id);
        if (!job.getUserId().equals(username) && !isCurrentUserAdmin()) {
            throw new ForbiddenOperationException("User does not own a job with this ID: " + job.getId());
        }
        jobService.deleteJob(username, job);

        return new ResponseEntity<>(HttpStatus.OK);
    }


    /**
     * Checks if the currently authenticated user is an admin.
     *
     * @return true if current user is an admin
     */
    private boolean isCurrentUserAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return authentication.getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));
    }
}
