package de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.dtos;

import de.unipassau.fim.ep.jobjogger.resourceserver.model.entities.notification.NotificationType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * Notification DTO passed to the client.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationDto {
    private Long id;
    private NotificationType notificationType;
    private Timestamp timestamp;
    private String userId;
    private boolean isSeen = false;
    private Long jobId;
    private String jobName;
    private String imageName;
    private String reason;
}
