FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=target/resource-server.jar
COPY ${JAR_FILE} app.jar
RUN apk update && \
    apk add --update bash && \
    apk add --update git
RUN git clone https://github.com/JWackerbauer/wait-for-it.git
RUN chmod +x wait-for-it/wait-for-it.sh
ENV WAIT_FOR_HOST=resource-db:3306
ENTRYPOINT bash wait-for-it/wait-for-it.sh $WAIT_FOR_HOST -t 120 && java -Djava.security.egd=file:/dev/./urandom -jar /app.jar